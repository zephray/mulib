//
// mu_gpio.c: Driver for MU GPIO IP
//
// Copyright 2024 Wenting Zhang <zephray@outlook.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
#include <string.h>
#include "mu_gpio.h"

void mu_gpio_init(mu_gpio_t *ctx, mu_gpio_inst_t *inst) {
    memset(ctx, 0, sizeof(*ctx));
    ctx->inst = inst;
    ctx->odmask = 0;
}

void mu_gpio_set_dir(mu_gpio_t *ctx, int pin_mask, mu_gpio_dir_t dir) {
    if (dir == GPIO_OUT) {
        ctx->inst->REG_OER = pin_mask;
        ctx->odmask &= ~pin_mask;
    }
    else if (dir == GPIO_IN) {
        ctx->inst->REG_OER &= ~pin_mask;
        ctx->odmask &= ~pin_mask;
    }
    else if (dir == GPIO_OD) {
        // Fake OD mode, set to input first and mark OD is enabled
        ctx->inst->REG_OER &= ~pin_mask;
        ctx->odmask |= pin_mask;
    }
}

void mu_gpio_put(mu_gpio_t *ctx, int pin_mask, int val) {
    if (val) {
        ctx->inst->REG_BSR = pin_mask;
    }
    else {
        ctx->inst->REG_BCR = pin_mask;
    }
}

int mu_gpio_get(mu_gpio_t *ctx, int pin_mask) {
    uint32_t rd = ctx->inst->REG_IDR;
    return !!(rd & pin_mask);
}
