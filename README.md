# mulib

Mushroom Reusable Library

Collection of IP cores used in my projects. See individual file for copyright information. Not and not going to be production ready.

## IPs

- mu_axirddma: Simple AXI4 read-only DMA with FIFO interface
- mu_axiwrdma: Simple AXI4 write-only DMA with FIFO interface
- mu_dcif: Digital camera interface controller
- mu_ddif: Digital display interface controller
- mu_dsilite: Basic MIPI-DSI display controller
- mu_epdif: Electrophoretic display interface controller
- mu_gpio: GPIO controller

### mu_dsilite

Basic MIPI DSI display controller

- Parameterized resolution and timing settings
- 2-lane operation only (can be extended to support 1-lane)
- RGB888 mode only (can be extended to support RGB565)
- Video mode only
- Optional CRC for LPa, ECC always enabled for SPa
- Optional return to LP every frame
- Integrated AXI4 DMA
- APB register interface
- Extreme light weight at <450 FF, <500 LUT6 and 0.5 BMEM on 7-series FPGA

## LICENSE

Other than the following exceptions, code are licensed under MIT.

The CRC module inside DSI controller is licensed under LGPL. It's possible to use the module without CRC however.
