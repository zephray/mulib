`timescale 1ns / 1ps
`default_nettype none
`include "mu_defines.vh"
//
// mu_epdif_lvds.v: LVDS adapter
//
// Copyright 2024 Wenting Zhang <zephray@outlook.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
module mu_epdif_lvds (
    input  wire         clk,
    input  wire         rst,
    output wire         locked,
    output wire         clk_pix,
    output wire         rst_pix,
    input  wire         lvds_en,
    input  wire         epdif_sdclk,
    input  wire [23:0]  epdif_sd,
    output wire         epd_sdclk_p,
    output wire         epd_sdclk_n,
    output wire [11:0]  epd_sd_p,
    output wire [11:0]  epd_sd_n
);
    wire clkfbout;
    wire clkout0;
    wire clkout1;
    wire clkout2;
    wire clkfbout_buf;
    wire mmcm_locked;
    MMCME2_ADV #(
        .BANDWIDTH            ("OPTIMIZED"),
        .CLKOUT4_CASCADE      ("FALSE"),
        .COMPENSATION         ("ZHOLD"),
        .STARTUP_WAIT         ("FALSE"),
        .DIVCLK_DIVIDE        (1),
        .CLKFBOUT_MULT_F      (20.000), // 1GHz
        .CLKFBOUT_PHASE       (0.000),
        .CLKFBOUT_USE_FINE_PS ("FALSE"),
        .CLKOUT0_DIVIDE_F     (20.000),  // 50MHz
        .CLKOUT0_PHASE        (0.000),
        .CLKOUT0_DUTY_CYCLE   (0.500),
        .CLKOUT0_USE_FINE_PS  ("FALSE"),
        .CLKOUT1_DIVIDE       (20.000),  // 50MHz
        .CLKOUT1_PHASE        (90.000),
        .CLKOUT1_DUTY_CYCLE   (0.500),
        .CLKOUT1_USE_FINE_PS  ("FALSE"),
        .CLKIN1_PERIOD        (20.00)
    ) mmcm_adv_inst (
        // Output clocks
        .CLKFBOUT            (clkfbout),
        .CLKFBOUTB           (),
        .CLKOUT0             (clkout0),
        .CLKOUT0B            (),
        .CLKOUT1             (clkout1),
        .CLKOUT1B            (),
        .CLKOUT2             (),
        .CLKOUT2B            (),
        .CLKOUT3             (),
        .CLKOUT3B            (),
        .CLKOUT4             (),
        .CLKOUT5             (),
        .CLKOUT6             (),
        // Input clock control
        .CLKFBIN             (clkfbout_buf),
        .CLKIN1              (clk),
        .CLKIN2              (1'b0),
        // Tied to always select the primary input clock
        .CLKINSEL            (1'b1),
        // Ports for dynamic reconfiguration
        .DADDR               (7'h0),
        .DCLK                (1'b0),
        .DEN                 (1'b0),
        .DI                  (16'h0),
        .DO                  (),
        .DRDY                (),
        .DWE                 (1'b0),
        // Ports for dynamic phase shift
        .PSCLK               (1'b0),
        .PSEN                (1'b0),
        .PSINCDEC            (1'b0),
        .PSDONE              (),
        // Other control and status signals
        .LOCKED              (mmcm_locked),
        .CLKINSTOPPED        (),
        .CLKFBSTOPPED        (),
        .PWRDWN              (1'b0),
        .RST                 (rst)
    );

    assign locked = mmcm_locked;

    BUFG clkfb_buf (
        .I(clkfbout),
        .O(clkfbout_buf)
    );

    wire fwd_clk;
    BUFG clkout0_bufr (
        .I(clkout0),
        .O(fwd_clk)
    );

    BUFG clkout1_bufr (
        .I(clkout1),
        .O(clk_pix)
    );

    xpm_cdc_sync_rst lcd_rst_sync(
        .dest_clk(clk_pix),
        .src_rst(!mmcm_locked),
        .dest_rst(rst_pix)
    );

    // Clock output
    OBUFTDS obuf_clk (
        .O(epd_sdclk_p),
        .OB(epd_sdclk_n),
        .I(fwd_clk),
        .T(!lvds_en)
    );

    genvar i;
    generate for (i = 0; i < 12; i = i + 1) begin
        wire dat_q;

        ODDR #(
            .DDR_CLK_EDGE("SAME_EDGE")
        ) oddr_dat (
            .Q(dat_q),
            .C(clk_pix),
            .CE(1'b1),
            .D1(epdif_sd[i]),
            .D2(epdif_sd[i+12]),
            .R(1'b0),
            .S(1'b0)
        );

        OBUFTDS obuf_dat (
            .O(epd_sd_p[i]),
            .OB(epd_sd_n[i]),
            .I(dat_q),
            .T(!lvds_en)
        );
    end endgenerate

endmodule
