`timescale 1ns / 1ps
`default_nettype none
`include "mu_defines.vh"
//
// mu_dsilite.v: Very basic DSI display controller
//
// Copyright 2024 Wenting Zhang <zephray@outlook.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
module mu_dsilite #(
    parameter AXI_AW = 32,
    parameter AXI_DW = 64,   // This module only works with 64-bit wide DW
    parameter HBP = 7'd64,
    parameter HFP = 7'd20,
    parameter HACT = 12'd480,
    parameter VBP = 7'd70,
    parameter VFP = 7'd40,
    parameter VACT = 12'd480,
    // Fine tuning
    parameter DSI_RTLP_WAIT     = 12'd512,
    parameter DSI_LP_BIT_LEN    = 12'd1,   // min 40+4 UI, max 85+6 UI ns
    parameter DSI_LP_MIN_WAIT   = 12'd8,
    parameter DSI_HS_EXIT_DELAY = 12'd2,
    parameter VBLK_RETURN_TO_LP = 1'b1,
    parameter SEND_CRC          = 1'b0,
    parameter DLY_HS_CLK_TO_DAT = 1'b0,
    parameter SEND_TURNON       = 1'b0
) (
    input  wire         clk,        // Clock
    input  wire         clk_pix,    // Pixel clock, 24MHz
    input  wire         rst,
    input  wire         rst_pix,
	// APB device port for register access
    /* verilator lint_off UNUSEDSIGNAL */
    `APB_SLAVE_IF,
    /* verilator lint_on UNUSEDSIGNAL */
	// AXI host port for direct memory access
    `AXI_MASTER_IF(AXI_AW, AXI_DW, 1), // Doesn't really need ID
	// Display interface
    input  wire         dpi_ext_vsync,
    // DSI LP, to IO mux
    output reg          dsi_lp_cp,
    output reg          dsi_lp_cn,
    output reg  [1:0]   dsi_lp_dp,
    output reg  [1:0]   dsi_lp_dn,
    // DSI HS, to HS serializer / PHY
    output wire [15:0]  dsi_hs_dat,
    output wire         dsi_hsck_ten,
    output wire         dsi_hsdat_ten
);

    /* verilator lint_off WIDTHEXPAND */
    localparam HACT_CYC = HACT / 2;
    localparam HACT_BYTES = HACT * 3;
    localparam BLKL_CYC = HBP + HFP + HACT * 3 / 2 + 6; 
    localparam VTOTAL = VBP + VFP + VACT;
    /* verilator lint_on WIDTHEXPAND */

    localparam DSILITE_REG_PCTL         = 16'h0;
    localparam DSILITE_REG_DMACTL       = 16'h4;
    localparam DSILITE_REG_FBSWAP       = 16'h8;
    localparam DSILITE_REG_STARTADDR0_L = 16'h20;
    localparam DSILITE_REG_ENDADDR0_L   = 16'h28;
    localparam DSILITE_REG_STARTADDR1_L = 16'h30;
    localparam DSILITE_REG_ENDADDR1_L   = 16'h38;

    /* verilator lint_off UNUSEDSIGNAL */
    reg [19:0] dsilite_pctl;
    reg [15:0] dsilite_dmactl;
    reg [31:0] dsilite_startaddr0_l;
    reg [31:0] dsilite_endaddr0_l;
    reg [31:0] dsilite_startaddr1_l;
    reg [31:0] dsilite_endaddr1_l;
    /* verilator lint_on UNUSEDSIGNAL */
    wire running_apb_clk; // running flag

    // Signals for register update
    wire fbswap_allowed;
    reg fbswap_allowed_pixclk;
    reg fbswap_done;
    reg fbswap_requested;

    mu_dbsync #(1) fbswap_sync (clk_pix, clk, fbswap_allowed_pixclk, fbswap_allowed);

    // APB Access
    always @(posedge clk) begin
        // Update registers
        if (fbswap_allowed && fbswap_requested && !fbswap_done) begin
            // Clear flag and swap framebuffer registers
            fbswap_requested <= 1'b0;
            fbswap_done <= 1'b1;
            dsilite_startaddr1_l <= dsilite_startaddr0_l;
            dsilite_endaddr1_l <= dsilite_endaddr0_l;
            dsilite_startaddr0_l <= dsilite_startaddr1_l;
            dsilite_endaddr0_l <= dsilite_endaddr1_l;
        end

        if (!fbswap_allowed)
            fbswap_done <= 1'b0;

        if (s_apb_penable && s_apb_pwrite && s_apb_psel) begin
            case (s_apb_paddr[15:0])
            DSILITE_REG_PCTL: dsilite_pctl <= s_apb_pwdata[19:0];
            DSILITE_REG_DMACTL: dsilite_dmactl <= s_apb_pwdata[15:0];
            DSILITE_REG_FBSWAP: fbswap_requested <= s_apb_pwdata[0];
            DSILITE_REG_STARTADDR0_L: dsilite_startaddr0_l <= s_apb_pwdata;
            DSILITE_REG_ENDADDR0_L: dsilite_endaddr0_l <= s_apb_pwdata;
            DSILITE_REG_STARTADDR1_L: dsilite_startaddr1_l <= s_apb_pwdata;
            DSILITE_REG_ENDADDR1_L: dsilite_endaddr1_l <= s_apb_pwdata;
            default: ; // nothing
            endcase
        end

        if (rst) begin
            dsilite_pctl <= 20'd0;
            dsilite_dmactl <= 16'd0;
            fbswap_requested <= 1'b0;
            fbswap_done <= 1'b0;
        end
    end

    assign s_apb_pready = 1'b1;
    reg [31:0] reg_rd;
    always @(*) begin
        case (s_apb_paddr[15:0])
        DSILITE_REG_PCTL: reg_rd = {running_apb_clk, 11'd0, dsilite_pctl};
        DSILITE_REG_DMACTL: reg_rd = {16'd0, dsilite_dmactl};
        DSILITE_REG_FBSWAP: reg_rd = {31'd0, fbswap_requested};
        DSILITE_REG_STARTADDR0_L: reg_rd = dsilite_startaddr0_l;
        DSILITE_REG_ENDADDR0_L: reg_rd = dsilite_endaddr0_l;
        DSILITE_REG_STARTADDR1_L: reg_rd = dsilite_startaddr1_l;
        DSILITE_REG_ENDADDR1_L: reg_rd = dsilite_endaddr1_l;
        default: reg_rd = 32'hdeadbeef;
        endcase
    end
    assign s_apb_prdata = reg_rd;

    // Register fields
    // Fields used in clk domain
    wire dc_swap_32b = dsilite_pctl[2]; // 0 - low 32b first, 1 - high 32b first
    wire dc_swap_rgbx = dsilite_pctl[3]; // 0 - xrgb, 1 - rgbx
    wire dc_swap_bgr = dsilite_pctl[4]; // 0 - rgb, 1 - bgr
    wire [7:0] ctl_burstlen = dsilite_dmactl[7:0];
    wire [7:0] ctl_maxinflight = dsilite_dmactl[15:8];

    // Fields used in clk_pix domain
    /* verilator lint_off UNUSEDSIGNAL */
    wire [19:0] dsilite_pctl_clk_pix;
    mu_dbsync #(20) pctl_sync (clk, clk_pix, dsilite_pctl, dsilite_pctl_clk_pix);
    wire tgen_en = dsilite_pctl_clk_pix[0];
    wire tgen_use_ext_vsync = dsilite_pctl_clk_pix[1];
    wire [7:0] tgen_ext_vsync_delay = dsilite_pctl_clk_pix[15:8];
    /* verilator lint_on UNUSEDSIGNAL */

    // DMA Data output
    wire [AXI_DW-1:0] dmard_dout;
    wire dmard_valid;
    wire dmard_ready;

    reg ctl_trigger; // internal DMA trigger
    /* verilator lint_off UNUSEDSIGNAL */
    // TODO: Collect information about error and underrun
    wire ctl_busy;
    wire ctl_error;
    /* verilator lint_on UNUSEDSIGNAL */

    wire ctl_trigger_apb_clk;
    mu_dsync trigger_sync (clk_pix, clk, ctl_trigger, ctl_trigger_apb_clk);

    mu_axirddma mu_axirddma (
        .clk(clk),
        .rst(rst),
        `AXI_CONN(m_, m_),
        // Data output
        .rd_dout(dmard_dout),
        .rd_valid(dmard_valid),
        .rd_ready(dmard_ready),
        // Control
        .ctl_startaddr(dsilite_startaddr0_l),
        .ctl_endaddr(dsilite_endaddr0_l),
        .ctl_burstlen(ctl_burstlen),
        .ctl_maxinflight(ctl_maxinflight),
        .ctl_trigger(ctl_trigger_apb_clk),
        .ctl_busy(ctl_busy),
        .ctl_error(ctl_error)
    ); 

    wire [23:0] p1h = dc_swap_rgbx ? dmard_dout[63:40] : dmard_dout[55:32];
    wire [23:0] p1l = dc_swap_rgbx ? dmard_dout[31:8] : dmard_dout[23:0];
    wire [23:0] p2h = dc_swap_bgr ? {p1h[7:0], p1h[15:8], p1h[23:16]} : p1h;
    wire [23:0] p2l = dc_swap_bgr ? {p1l[7:0], p1l[15:8], p1l[23:16]} : p1l;
    wire [47:0] dmard_pix = dc_swap_32b ? {p2h, p2l} : {p2l, p2h};

    wire [23:0] dmapix_data;
    wire dmapix_valid;
    wire dmapix_ready;
    mu_widthadapt_2_to_1 #(
        .IW(48),
        .OW(24)
    ) pix_width_adapt (
        .clk(clk),
        .rst(rst),
        // Incoming port
        .wr_data(dmard_pix),
        .wr_valid(dmard_valid),
        .wr_ready(dmard_ready),
        // Outgoing port
        .rd_data(dmapix_data),
        .rd_valid(dmapix_valid),
        .rd_ready(dmapix_ready)
    );

    reg scan_fifo_ready;
    wire [23:0] scan_pixel;

    wire scan_fifo_valid;
    mu_fifo_async #(.DW(24), .DEPTH(512)) pixel_fifo (
        .wr_clk(clk),
        .wr_nreset(!rst),
        .wr_din(dmapix_data),
        .wr_valid(dmapix_valid),
        .wr_ready(dmapix_ready),
        .rd_clk(clk_pix),
        .rd_nreset(!rst_pix),
        .rd_dout(scan_pixel),
        .rd_ready(scan_fifo_ready),
        .rd_valid(scan_fifo_valid)
    );

    // Timing and Packet Gen
    localparam DSI_VSYNC_START  = 8'h01;
    //localparam DSI_VSYNC_END    = 8'h11;
    localparam DSI_HSYNC_START  = 8'h21;
    //localparam DSI_HSYNC_END    = 8'h31;
    //localparam DSI_NULL         = 8'h09;
    localparam DSI_BLANKING     = 8'h19;
    localparam DSI_PIXEL_24BPP  = 8'h3e;
    localparam DSI_END_OF_XMIT  = 8'h08;
    localparam DSI_TURNON       = 8'h32;
    //localparam DSI_SHUTDOWN     = 8'h22;

    localparam DSI_SYNC_WORD    = 8'b10111000; //reversed of 00011101

    reg [15:0] dsi_outbuf; // Lower: lane 0 (DI), higher: lane 1 (WC LSB)
    reg [7:0] dsi_par2buf; // WC MSB
    wire [7:0] dsi_ecc;
    reg dsi_d_hsen;
    reg dsi_c_hsen;

    reg [11:0] scan_v_cnt;
    reg [11:0] scan_h_cnt;

    reg pkt_is_pix;
    reg [11:0] data_cnt;
    reg send_init;

    wire [15:0] dsi_crc;
    /* verilator lint_off UNUSEDSIGNAL */
    reg dsi_crc_dvalid; // Used only if CRC is enabled
    /* verilator lint_on UNUSEDSIGNAL */

    reg [3:0] scan_state;
    localparam SCAN_IDLE = 4'd0;
    localparam SCAN_CLK_HS_01 = 4'd1;
    localparam SCAN_CLK_HS_00 = 4'd2;
    localparam SCAN_DAT_LP_WAIT = 4'd3;
    localparam SCAN_DAT_HS_01 = 4'd4;
    localparam SCAN_DAT_HS_00 = 4'd5;
    localparam SCAN_DAT_HS_WAIT = 4'd6;
    localparam SCAN_DAT_HS_PKT1 = 4'd7;
    localparam SCAN_DAT_HS_PKT2 = 4'd8;
    localparam SCAN_DAT_HS_BLK = 4'd9;
    localparam SCAN_DAT_HS_PIX1 = 4'd10; // 3 cycles 2 pixels
    localparam SCAN_DAT_HS_PIX2 = 4'd11;
    localparam SCAN_DAT_HS_PIX3 = 4'd12;
    localparam SCAN_DAT_HS_CRC = 4'd13;
    localparam SCAN_DAT_HS_EXIT = 4'd14;

    /* verilator lint_off WIDTHEXPAND */
    //wire scan_in_vbp = (scan_v_cnt < (vsync + vbp));
    wire scan_in_vact = (scan_v_cnt >= VBP) && (scan_v_cnt < (VBP + VACT));
    //wire scan_in_vfp = (scan_v_cnt >= (vsync + vbp + vact));
    /* verilator lint_on WIDTHEXPAND */

    wire running = scan_state != SCAN_IDLE;
    mu_dbsync #(1) running_flag_sync (clk_pix, clk, running, running_apb_clk);

    always @(posedge clk_pix) begin
        case (scan_state)
        SCAN_IDLE: begin
            if (tgen_en) begin
                if (dsi_c_hsen) begin
                    // Clock lane already in HS,
                    scan_state <= SCAN_DAT_LP_WAIT;
                    ctl_trigger <= 1'b1;
                    fbswap_allowed_pixclk <= 1'b0;
                end
                else begin
                    // Bring clock lane into HS first
                    dsi_lp_cp <= 1'b0;
                    dsi_lp_cn <= 1'b1;
                    scan_state <= SCAN_CLK_HS_01;
                end
            end
            scan_h_cnt <= 'd0;
            scan_v_cnt <= 'd0;
        end
        SCAN_CLK_HS_01: begin
            scan_h_cnt <= scan_h_cnt + 'd1;
            if (scan_h_cnt == DSI_LP_BIT_LEN) begin
                scan_h_cnt <= 'd0;
                dsi_lp_cp <= 1'b0;
                dsi_lp_cn <= 1'b0;
                scan_state <= SCAN_CLK_HS_00;
            end
        end
        SCAN_CLK_HS_00: begin
            scan_h_cnt <= scan_h_cnt + 'd1;
            if (scan_h_cnt == DSI_LP_BIT_LEN) begin
                scan_h_cnt <= 'd0;
                dsi_c_hsen <= 1'b1;
                if (DLY_HS_CLK_TO_DAT) begin
                    scan_state <= SCAN_DAT_LP_WAIT;
                end
                else begin
                    dsi_lp_dp <= 2'b00;
                    dsi_lp_dn <= 2'b11;
                    scan_state <= SCAN_DAT_HS_01;
                    ctl_trigger <= 1'b1;
                    fbswap_allowed_pixclk <= 1'b0;
                end
            end
        end
        SCAN_DAT_LP_WAIT: begin
            scan_h_cnt <= scan_h_cnt + 'd1;
            if (scan_h_cnt == DSI_RTLP_WAIT) begin
                scan_h_cnt <= 'd0;
                dsi_lp_dp <= 2'b00;
                dsi_lp_dn <= 2'b11;
                scan_state <= SCAN_DAT_HS_01;
            end
        end
        SCAN_DAT_HS_01: begin
            scan_h_cnt <= scan_h_cnt + 'd1;
            if (scan_h_cnt == DSI_LP_BIT_LEN) begin
                scan_h_cnt <= 'd0;
                dsi_lp_dp <= 2'b00;
                dsi_lp_dn <= 2'b00;
                scan_state <= SCAN_DAT_HS_00;
            end
        end
        SCAN_DAT_HS_00: begin
            scan_h_cnt <= scan_h_cnt + 'd1;
            if (scan_h_cnt == DSI_LP_BIT_LEN) begin
                scan_h_cnt <= 'd0;
                dsi_outbuf <= 'd0;
                dsi_d_hsen <= 1'b1;
                scan_state <= SCAN_DAT_HS_WAIT;
            end
        end
        SCAN_DAT_HS_WAIT: begin
            scan_h_cnt <= scan_h_cnt + 'd1;
            if (scan_h_cnt == DSI_LP_MIN_WAIT) begin
                scan_h_cnt <= 'd0;
                dsi_outbuf <= {2{DSI_SYNC_WORD}};
                scan_state <= SCAN_DAT_HS_PKT1;
                if (SEND_TURNON)
                    send_init <= 1'b1;
            end
        end
        SCAN_DAT_HS_PKT1: begin
            scan_h_cnt <= scan_h_cnt + 'd1;
            if (SEND_TURNON && send_init) begin
                send_init <= 1'b0;
                data_cnt <= 'd0;
                pkt_is_pix <= 1'b0;
                dsi_outbuf <= {8'd0, DSI_TURNON};
                dsi_par2buf <= 8'd0;
                scan_h_cnt <= 'd0;
            end
            if (scan_h_cnt == 'd0) begin
                // Always start with send a sync packet
                if (scan_v_cnt == 'd0) begin
                    // Send vsync
                    data_cnt <= 'd0;
                    pkt_is_pix <= 1'b0;
                    dsi_outbuf <= {8'd0, DSI_VSYNC_START};
                    dsi_par2buf <= 8'd0;
                end
                else begin
                    // Send hsync
                    data_cnt <= 'd0;
                    pkt_is_pix <= 1'b0;
                    dsi_outbuf <= {8'd0, DSI_HSYNC_START};
                    dsi_par2buf <= 8'd0;
                end
            end
            else begin
                // Depends on the line
                if (scan_in_vact) begin
                    // Pixel lines
                    scan_h_cnt <= scan_h_cnt + 'd1;
                    if (scan_h_cnt == 'd1) begin
                        // HBP
                        data_cnt <= {5'b0, HBP};
                        pkt_is_pix <= 1'b0;
                        dsi_outbuf <= {HBP[6:0], 1'b0, DSI_BLANKING};
                        dsi_par2buf <= 8'd0;
                    end
                    else if (scan_h_cnt == 'd2) begin
                        // Valid pixel
                        data_cnt <= HACT_CYC;
                        pkt_is_pix <= 1'b1;
                        dsi_outbuf <= {HACT_BYTES[7:0], DSI_PIXEL_24BPP};
                        dsi_par2buf <= {4'd0, HACT_BYTES[11:8]};
                    end
                    else begin
                        // HFP
                        data_cnt <= {5'b0, HFP};
                        pkt_is_pix <= 1'b0;
                        dsi_outbuf <= {HFP[6:0], 1'b0, DSI_BLANKING};
                        dsi_par2buf <= 8'd0;
                        // Line also ends
                        scan_h_cnt <= 'd0;
                        scan_v_cnt <= scan_v_cnt + 'd1;
                        ctl_trigger <= 1'b0;
                    end
                end
                else if ((scan_v_cnt == VTOTAL) && VBLK_RETURN_TO_LP) begin
                    // Finished
                    scan_h_cnt <= 'd0;
                    data_cnt <= 'd0;
                    pkt_is_pix <= 1'b0;
                    dsi_outbuf <= {8'd0, DSI_END_OF_XMIT};
                    dsi_par2buf <= 8'd0;
                end
                else begin
                    // Normal Blanking lines
                    scan_h_cnt <= 'd0;
                    scan_v_cnt <= scan_v_cnt + 'd1;
                    data_cnt <= BLKL_CYC;
                    pkt_is_pix <= 1'b0;
                    dsi_outbuf <= {BLKL_CYC[6:0], 1'b0, DSI_BLANKING};
                    dsi_par2buf <= {3'd0, BLKL_CYC[11:7]};
                    if ((scan_v_cnt == VTOTAL - 'd1) && !VBLK_RETURN_TO_LP)
                        scan_v_cnt <= 'd0;
                    if (!ctl_busy) begin
                        // DMA finished, and is in blanking, allow framebuffer
                        // swapping to happen
                        fbswap_allowed_pixclk <= 1'b1;
                    end
                end
            end
            scan_state <= SCAN_DAT_HS_PKT2;
        end
        SCAN_DAT_HS_PKT2: begin
            dsi_outbuf <= {dsi_ecc, dsi_par2buf};
            if (data_cnt != 'd0) begin
                // LPa, enter data phase
                if (pkt_is_pix) begin
                    scan_state <= SCAN_DAT_HS_PIX1;
                    scan_fifo_ready <= 1'b1;
                end
                else begin
                    scan_state <= SCAN_DAT_HS_BLK;
                end
            end
            else begin
                // SPa
                if ((scan_v_cnt == VTOTAL) &&
                        (VBLK_RETURN_TO_LP || !tgen_en)) begin
                    // Exit
                    scan_state <= SCAN_DAT_HS_EXIT;
                end
                else begin
                    // send next packet
                    scan_state <= SCAN_DAT_HS_PKT1;
                end
            end
        end
        SCAN_DAT_HS_BLK: begin
            dsi_outbuf <= 'd0;
            dsi_crc_dvalid <= 1'b1;
            data_cnt <= data_cnt - 'd1;
            if (data_cnt == 'd1) begin
                scan_state <= SCAN_DAT_HS_CRC;
            end
        end
        SCAN_DAT_HS_PIX1: begin
            // RG pixel 1
            dsi_outbuf <= {scan_pixel[15:8], scan_pixel[23:16]};
            dsi_par2buf <= scan_pixel[7:0];
            scan_fifo_ready <= 1'b0;
            scan_state <= SCAN_DAT_HS_PIX2;
            dsi_crc_dvalid <= 1'b1;
        end
        SCAN_DAT_HS_PIX2: begin
            // BR pixel 1 2
            dsi_outbuf <= {scan_pixel[23:16], dsi_par2buf};
            scan_fifo_ready <= 1'b1;
            scan_state <= SCAN_DAT_HS_PIX3;
            dsi_crc_dvalid <= 1'b1;
        end
        SCAN_DAT_HS_PIX3: begin
            // GB pixel 2
            dsi_outbuf <= {scan_pixel[7:0], scan_pixel[15:8]};
            data_cnt <= data_cnt - 'd1;
            dsi_crc_dvalid <= 1'b1;
            if (data_cnt == 'd1) begin
                scan_state <= SCAN_DAT_HS_CRC;
                scan_fifo_ready <= 1'b0;
            end
            else begin
                scan_state <= SCAN_DAT_HS_PIX1;
                scan_fifo_ready <= 1'b1;
            end
        end
        SCAN_DAT_HS_CRC: begin
            dsi_outbuf <= dsi_crc; // We don't do that here
            dsi_crc_dvalid <= 1'b0;
            scan_state <= SCAN_DAT_HS_PKT1;
        end
        SCAN_DAT_HS_EXIT: begin
            scan_h_cnt <= scan_h_cnt + 'd1;
            if (scan_h_cnt == DSI_HS_EXIT_DELAY) begin
                dsi_d_hsen <= 1'b0;
                dsi_lp_dp <= 2'b11;
                dsi_lp_dn <= 2'b11;
                scan_h_cnt <= 'd0;
                scan_v_cnt <= 'd0;
                if (tgen_en) begin
                    scan_state <= SCAN_DAT_LP_WAIT;
                    ctl_trigger <= 1'b1;
                    fbswap_allowed_pixclk <= 1'b0;
                end
                else begin
                    scan_state <= SCAN_IDLE;
                    // TODO: Properly stop clock lane
                    dsi_c_hsen <= 1'b0;
                    dsi_lp_cp <= 1'b1;
                    dsi_lp_cn <= 1'b1;
                end
            end
        end
        default: begin
            // Invalid state
            $display("Scan FSM in invalid state");
            scan_state <= SCAN_IDLE;
        end
        endcase

        if (rst_pix) begin
            scan_state <= SCAN_IDLE;
            scan_h_cnt <= 0;
            scan_v_cnt <= 0;
            scan_fifo_ready <= 1'b0;
            dsi_d_hsen <= 1'b0;
            dsi_c_hsen <= 1'b0;
            dsi_lp_cp <= 1'b1;
            dsi_lp_cn <= 1'b1;
            dsi_lp_dp <= 2'b11;
            dsi_lp_dn <= 2'b11;
            dsi_crc_dvalid <= 1'b0;
        end
    end

    assign dsi_hsdat_ten = !dsi_d_hsen;
    assign dsi_hsck_ten = !dsi_c_hsen;
    assign dsi_hs_dat = dsi_outbuf;

    mu_dsiecc mu_dsiecc (
        .d({dsi_par2buf, dsi_outbuf}),
        .ecc({dsi_ecc})
    );

    generate if (SEND_CRC) begin: gen_crc
        mu_dsicrc mu_dsicrc (
            .clk(clk_pix),
            .rst(!dsi_crc_dvalid),
            .data({dsi_outbuf[7:0], dsi_outbuf[15:8]}),
            .data_valid(dsi_crc_dvalid),
            .crc(dsi_crc)
        );
    end else begin: gen_crc_tieoff
        assign dsi_crc = 16'h0000;
    end endgenerate

endmodule

`default_nettype wire
