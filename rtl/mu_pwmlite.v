`timescale 1ns / 1ps
`default_nettype none
`include "mu_defines.vh"
//
// mu_pwmlite.v: Very basic PWM generator
//
// Copyright 2024 Wenting Zhang <zephray@outlook.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
module mu_pwmlite #(
    NCH = 2,
    PSCW = 12,
    CNTW = 12
) (
    input wire              clk,
    input wire              rst,
    // APB device port for register access
    /* verilator lint_off UNUSEDSIGNAL */
    `APB_SLAVE_IF,
    /* verilator lint_on UNUSEDSIGNAL */
    output reg [NCH-1:0]    pwm_out
);

    localparam PWMLITE_REG_CTL  = 4'h0;
    localparam PWMLITE_REG_PSC  = 4'h4;
    localparam PWMLITE_REG_CNT  = 4'h8;
    localparam PWMLITE_REG_CMP  = 4'hC;

    genvar i;
    generate for (i = 0; i < NCH; i = i + 1) begin: gen_pwm_ch
        reg pwm_en;
        reg [PSCW-1:0] pwm_psc;
        reg [CNTW-1:0] pwm_cnt;
        reg [CNTW-1:0] pwm_cmp;

        wire match = (s_apb_paddr[15:4] == i) && s_apb_penable && s_apb_psel;
        always @(posedge clk) begin
            if (match && s_apb_pwrite) begin
                case (s_apb_paddr[3:0])
                PWMLITE_REG_CTL: pwm_en <= s_apb_pwdata[0];
                PWMLITE_REG_PSC: pwm_psc <= s_apb_pwdata[PSCW-1:0];
                PWMLITE_REG_CNT: pwm_cnt <= s_apb_pwdata[CNTW-1:0];
                PWMLITE_REG_CMP: pwm_cmp <= s_apb_pwdata[CNTW-1:0];
                default: ;
                endcase
            end

            if (rst) begin
                pwm_en <= 'd0;
            end
        end

        assign s_apb_pready = 1'b1;
        assign s_apb_prdata = 32'hdeadbeef; // No readback

        reg [PSCW-1:0] psc_cntr;
        reg [CNTW-1:0] pwm_cntr;
        always @(posedge clk) begin
            if (pwm_en) begin
                psc_cntr <= psc_cntr - 'd1;
                if (psc_cntr == 'd0) begin
                    psc_cntr <= pwm_psc;

                    // Counting
                    pwm_cntr <= pwm_cntr - 'd1;
                    if (pwm_cntr == 'd0)
                        pwm_cntr <= pwm_cnt;
                    // Output compare
                    pwm_out[i] <= (pwm_cntr < pwm_cmp);
                end
            end

            if (rst) begin
                psc_cntr <= 'd0;
                pwm_cntr <= 'd0;
                pwm_out[i] <= 1'b0;
            end
        end

    end endgenerate


endmodule
