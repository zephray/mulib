`timescale 1ns / 1ps
`default_nettype none
`include "mu_defines.vh"
//
// mu_dcif.v: Digital camera interface controller
//
// Copyright 2024 Wenting Zhang <zephray@outlook.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
module mu_dcif #(
    parameter AXI_AW = 32,
    parameter AXI_DW = 64,   // This module only works with 64-bit wide DW
    parameter HCNTW = 16,
    parameter VCNTW = 12
) (
    input  wire         clk,        // Clock
    input  wire         rst,
    // APB device port for register access
    /* verilator lint_off UNUSEDSIGNAL */
    `APB_SLAVE_IF,
    /* verilator lint_on UNUSEDSIGNAL */
    // AXI host port for direct memory access
    `AXI_MASTER_IF(AXI_AW, AXI_DW, 1), // Doesn't really need ID
    // Camera interface
    input  wire [15:0]  dvp_d,
    input  wire         dvp_hsync,
    input  wire         dvp_vsync,
    input  wire         dvp_pclk,
    input  wire         dvp_rst,
    output wire [63:0]  debug
);

    localparam DCIF_REG_PCTL            = 16'h0;
    localparam DCIF_REG_DMACTL          = 16'h4;
    localparam DCIF_REG_FBSWAP          = 16'h8;
    localparam DCIF_REG_STATUS          = 16'hC;
    localparam DCIF_REG_VBLK            = 16'h10;
    localparam DCIF_REG_VACT            = 16'h14;
    localparam DCIF_REG_HBLK            = 16'h18;
    localparam DCIF_REG_HACT            = 16'h1C;
    localparam DCIF_REG_STARTADDR0_L    = 16'h20;
    localparam DCIF_REG_ENDADDR0_L      = 16'h28;
    localparam DCIF_REG_STARTADDR1_L    = 16'h30;
    localparam DCIF_REG_ENDADDR1_L      = 16'h38;

    /* verilator lint_off UNUSEDSIGNAL */
    reg [31:0] dcif_pctl;
    reg [15:0] dcif_dmactl;
    reg [VCNTW-1:0] dcif_vblk;
    reg [VCNTW-1:0] dcif_vact;
    reg [HCNTW-1:0] dcif_hblk;
    reg [HCNTW-1:0] dcif_hact;
    reg [31:0] dcif_startaddr0_l;
    reg [31:0] dcif_endaddr0_l;
    reg [31:0] dcif_startaddr1_l;
    reg [31:0] dcif_endaddr1_l;
    /* verilator lint_on UNUSEDSIGNAL */

    wire [1:0] state_apb_clk;

    // Signals for register update
    reg fbswap_allowed;
    reg fbswap_done;
    reg fbswap_requested;


    // APB Access
    always @(posedge clk) begin
        // Update registers
        if (fbswap_allowed && fbswap_requested && !fbswap_done) begin
            // Clear flag and swap framebuffer registers
            fbswap_requested <= 1'b0;
            fbswap_done <= 1'b1;
            dcif_startaddr1_l <= dcif_startaddr0_l;
            dcif_endaddr1_l <= dcif_endaddr0_l;
            dcif_startaddr0_l <= dcif_startaddr1_l;
            dcif_endaddr0_l <= dcif_endaddr1_l;
        end

        if (!fbswap_allowed)
            fbswap_done <= 1'b0;

        if (s_apb_penable && s_apb_pwrite && s_apb_psel) begin
            case (s_apb_paddr[15:0])
            DCIF_REG_PCTL: dcif_pctl <= s_apb_pwdata;
            DCIF_REG_DMACTL: dcif_dmactl <= s_apb_pwdata[15:0];
            DCIF_REG_FBSWAP: fbswap_requested <= s_apb_pwdata[0];
            DCIF_REG_VBLK: dcif_vblk <= s_apb_pwdata[VCNTW-1:0];
            DCIF_REG_VACT: dcif_vact <= s_apb_pwdata[VCNTW-1:0];
            DCIF_REG_HBLK: dcif_hblk <= s_apb_pwdata[HCNTW-1:0];
            DCIF_REG_HACT: dcif_hact <= s_apb_pwdata[HCNTW-1:0];
            DCIF_REG_STARTADDR0_L: dcif_startaddr0_l <= s_apb_pwdata;
            DCIF_REG_ENDADDR0_L: dcif_endaddr0_l <= s_apb_pwdata;
            DCIF_REG_STARTADDR1_L: dcif_startaddr1_l <= s_apb_pwdata;
            DCIF_REG_ENDADDR1_L: dcif_endaddr1_l <= s_apb_pwdata;
            default: ; // nothing
            endcase
        end

        if (rst) begin
            dcif_pctl <= 32'd0;
            dcif_dmactl <= 16'd0;
            fbswap_requested <= 1'b0;
            fbswap_done <= 1'b0;
        end
    end

    assign s_apb_pready = 1'b1;
    reg [31:0] reg_rd;
    always @(*) begin
        case (s_apb_paddr[15:0])
        DCIF_REG_PCTL: reg_rd = dcif_pctl;
        DCIF_REG_DMACTL: reg_rd = {16'd0, dcif_dmactl};
        DCIF_REG_FBSWAP: reg_rd = {31'd0, fbswap_requested};
        DCIF_REG_STATUS: reg_rd = {30'd0, state_apb_clk};
        DCIF_REG_VBLK: reg_rd = {{(32-VCNTW){1'b0}}, dcif_vblk};
        DCIF_REG_VACT: reg_rd = {{(32-VCNTW){1'b0}}, dcif_vact};
        DCIF_REG_HBLK: reg_rd = {{(32-HCNTW){1'b0}}, dcif_hblk};
        DCIF_REG_HACT: reg_rd = {{(32-HCNTW){1'b0}}, dcif_hact};
        DCIF_REG_STARTADDR0_L: reg_rd = dcif_startaddr0_l;
        DCIF_REG_ENDADDR0_L: reg_rd = dcif_endaddr0_l;
        DCIF_REG_STARTADDR1_L: reg_rd = dcif_startaddr1_l;
        DCIF_REG_ENDADDR1_L: reg_rd = dcif_endaddr1_l;
        default: reg_rd = 32'hdeadbeef;
        endcase
    end
    assign s_apb_prdata = reg_rd;

    // Register fields
    wire [7:0] dmactl_burstlen = dcif_dmactl[7:0];
    wire [7:0] dmactl_maxinflight = dcif_dmactl[15:8];


    // Input sampling and timing counter
    // On Xilinx devices, the input pixel clock should be buffered prior to
    // entering this block.
    /* verilator lint_off UNUSEDSIGNAL */
    wire [31:0] dcif_pctl_clk_pix;
    wire [VCNTW-1:0] dcif_vblk_clk_pix_pre;
    wire [VCNTW-1:0] dcif_vact_clk_pix_pre;
    wire [HCNTW-1:0] dcif_hblk_clk_pix_pre;
    wire [HCNTW-1:0] dcif_hact_clk_pix_pre;
    /* verilator lint_on UNUSEDSIGNAL */
    mu_dbsync #(32) pctl_sync (clk, dvp_pclk, dcif_pctl, dcif_pctl_clk_pix);
    mu_dbsync #(VCNTW) vblk_sync (clk, dvp_pclk, dcif_vblk, dcif_vblk_clk_pix_pre);
    mu_dbsync #(VCNTW) vact_sync (clk, dvp_pclk, dcif_vact, dcif_vact_clk_pix_pre);
    mu_dbsync #(HCNTW) hblk_sync (clk, dvp_pclk, dcif_hblk, dcif_hblk_clk_pix_pre);
    mu_dbsync #(HCNTW) hact_sync (clk, dvp_pclk, dcif_hact, dcif_hact_clk_pix_pre);

    reg [VCNTW-1:0] dcif_vblk_clk_pix;
    reg [VCNTW-1:0] dcif_vact_clk_pix;
    reg [HCNTW-1:0] dcif_hblk_clk_pix;
    reg [HCNTW-1:0] dcif_hact_clk_pix;

    /* verilator lint_off WIDTHTRUNC */
    /* verilator lint_off WIDTHEXPAND */
    wire [HCNTW-1:0] htotal_clk_pix = dcif_hblk_clk_pix + dcif_hact_clk_pix;
    wire [VCNTW-1:0] vtotal_clk_pix = dcif_vblk_clk_pix + dcif_vact_clk_pix;
    /* verilator lint_on WIDTHTRUNC */
    /* verilator lint_on WIDTHEXPAND */

    wire ctl_en = dcif_pctl_clk_pix[0];
    wire ctl_use_vsync = dcif_pctl_clk_pix[1];
    wire ctl_use_hsync = dcif_pctl_clk_pix[2];
    wire ctl_vsync_pol = dcif_pctl_clk_pix[16];
    wire ctl_hsync_pol = dcif_pctl_clk_pix[17];
    wire ctl_mask_even_pix_pre = dcif_pctl_clk_pix[18]; // Mask out even pixels
    wire ctl_mask_odd_pix_pre = dcif_pctl_clk_pix[19]; // Mask out odd pixels

    reg ctl_mask_even_pix;
    reg ctl_mask_odd_pix;

    // Flop in external signals
    reg hsync;
    reg vsync;
    reg [15:0] din;
    always @(posedge dvp_pclk) begin
        hsync <= dvp_hsync ^ ctl_hsync_pol;
        vsync <= dvp_vsync ^ ctl_vsync_pol;
        din <= dvp_d;
    end

    // Additional flop for edge detection
    reg last_hsync;
    reg last_vsync;
    always @(posedge dvp_pclk) begin
        last_hsync <= hsync;
        last_vsync <= vsync;
    end

    reg [VCNTW-1:0] v_cnt;
    reg [HCNTW-1:0] h_cnt;

    reg [1:0] state;
    localparam STATE_IDLE = 2'd0;
    localparam STATE_WAITVSYNC = 2'd1;
    localparam STATE_RUNNING = 2'd2;
    
    mu_dbsync #(2) state_sync (dvp_pclk, clk, state, state_apb_clk);

    reg dma_enable_clk_pix;

    reg [15:0] pixin_data;
    reg pixin_valid;

    /* verilator lint_off WIDTHEXPAND */
    wire in_vblk = (v_cnt < dcif_vblk_clk_pix);
    wire in_vact = !in_vblk && (v_cnt < vtotal_clk_pix);
    wire in_hblk = (h_cnt < dcif_hblk_clk_pix);
    wire in_hact = !in_hblk && (h_cnt < htotal_clk_pix);
    /* verilator lint_on WIDTHEXPAND */

    always @(posedge dvp_pclk) begin
        case (state)
        STATE_IDLE: begin
            dma_enable_clk_pix <= 1'b0;
            if (ctl_en) begin
                state <= STATE_WAITVSYNC;
                dcif_vblk_clk_pix <= dcif_vblk_clk_pix_pre;
                dcif_vact_clk_pix <= dcif_vact_clk_pix_pre;
                dcif_hblk_clk_pix <= dcif_hblk_clk_pix_pre;
                dcif_hact_clk_pix <= dcif_hact_clk_pix_pre;
                ctl_mask_even_pix <= ctl_mask_even_pix_pre;
                ctl_mask_odd_pix <= ctl_mask_odd_pix_pre;
            end
        end
        STATE_WAITVSYNC: begin
            if ((!last_vsync && vsync) || !ctl_use_vsync) begin
                state <= STATE_RUNNING;
                v_cnt <= 'd0;
                h_cnt <= 'd0;
                dma_enable_clk_pix <= 1'b1;
            end
            else if (!ctl_en) begin
                state <= STATE_IDLE;
            end
        end
        STATE_RUNNING: begin
            // Counter operation
            h_cnt <= h_cnt + 'd1;
            if (((h_cnt == htotal_clk_pix - 'd1) && !ctl_use_hsync)
                    || (!last_hsync && hsync && ctl_use_hsync)) begin
                h_cnt <= 'd0;
                v_cnt <= v_cnt + 'd1;
                if (v_cnt == vtotal_clk_pix - 'd1) begin
                    v_cnt <= 'd0;
                    state <= STATE_IDLE;
                    // TODO: This requires the incoming stream has a non-zero VFP
                end
            end
            if (in_vact && in_hact) begin
                pixin_valid <= (h_cnt[0] && !ctl_mask_odd_pix) ||
                    (!h_cnt[0] && !ctl_mask_even_pix);
                pixin_data <= din;
            end
            else begin
                pixin_valid <= 1'b0;
            end
        end
        default: begin
            // Invalid state
        end
        endcase

        if (dvp_rst) begin
            state <= STATE_IDLE;
            dma_enable_clk_pix <= 1'b0;
        end
    end

    // 16 bit to 64 bit conversion
    wire [63:0] pixin64_data;
    wire pixin64_ready;
    wire pixin64_valid;
    mu_widthadapt_1_to_4 #(
        .IW(16),
        .OW(64),
        .ORDER(1)
    ) pix_width_adapt (
        .clk(dvp_pclk),
        .rst(dvp_rst),
        .wr_data(pixin_data),
        .wr_valid(pixin_valid),
        .wr_ready(), // TODO: Overrun detection
        .rd_data(pixin64_data),
        .rd_valid(pixin64_valid),
        .rd_ready(pixin64_ready)
    );

    // Async FIFO back to AXI clock domain
    wire [63:0] fifo_pixel;
    wire fifo_valid;
    wire fifo_ready;
    mu_fifo_async #(.DW(64), .DEPTH(256)) pixel_fifo (
        .wr_clk(dvp_pclk),
        .wr_nreset(!dvp_rst),
        .wr_din(pixin64_data),
        .wr_valid(pixin64_valid),
        .wr_ready(pixin64_ready),
        .rd_clk(clk),
        .rd_nreset(!rst),
        .rd_dout(fifo_pixel),
        .rd_ready(fifo_ready),
        .rd_valid(fifo_valid)
    );

    // Enable signal CDC
    wire dma_enable;
    mu_dsync en_sync (dvp_pclk, clk, dma_enable_clk_pix, dma_enable);

    // AXI DMA
    reg dma_trigger;
    wire dma_busy;
    wire dma_error;
    mu_axiwrdma #(.AXI_AW(AXI_AW), .AXI_DW(AXI_DW)) axiwrdma (
        .clk(clk),
        .rst(rst),
        `AXI_CONN(m_, m_),
        .wr_din(fifo_pixel),
        .wr_valid(fifo_valid),
        .wr_ready(fifo_ready),
        .ctl_startaddr(dcif_startaddr0_l),
        .ctl_endaddr(dcif_endaddr0_l),
        .ctl_burstlen(dmactl_burstlen),
        .ctl_maxinflight(dmactl_maxinflight),
        .ctl_trigger(dma_trigger),
        .ctl_busy(dma_busy),
        .ctl_error(dma_error)
    );

    // DMA state management
    reg dma_triggered;
    always @(posedge clk) begin
        if (dma_enable && !dma_trigger && !dma_triggered) begin
            // DMA should be triggered
            dma_trigger <= 1'b1;
            fbswap_allowed <= 1'b0;
        end

        if (dma_trigger && !dma_triggered && dma_busy) begin
            // DMA started
            dma_triggered <= 1'b1;
            dma_trigger <= 1'b0;
        end

        if (dma_triggered && !dma_busy && !dma_enable) begin
            // DMA finished
            dma_triggered <= 1'b0;
            fbswap_allowed <= 1'b1;
        end

        if (rst) begin
            dma_trigger <= 1'b0;
            fbswap_allowed <= 1'b0;
        end
    end

    assign debug[11:0] = h_cnt[11:0];
    assign debug[23:12] = v_cnt[11:0];
    assign debug[25:24] = state;
    assign debug[26] = dma_enable;
    assign debug[27] = dma_trigger;
    assign debug[28] = dma_busy;
    assign debug[29] = fbswap_allowed;
    assign debug[30] = fbswap_requested;
    assign debug[31] = fbswap_done;
    assign debug[63:32] = dcif_endaddr0_l;

endmodule

`default_nettype wire
