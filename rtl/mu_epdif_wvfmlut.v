`timescale 1ns / 1ps
`default_nettype none
//
// wvfmlut.v Waveform lookup table SRAM wrapper
//
// Copyright 2024 Wenting Zhang <zephray@outlook.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
module mu_epdif_wvfmlut #(
    parameter AW = 14, // In 2-bit words
    parameter DW = 2, // In output bits
    parameter RDW = DW * 4 // RAM bit width
) (
    input wire clk,
    // Write port, in 8bit
    input wire we,
    input wire [AW-3:0] addr,
    input wire [RDW-1:0] din,
    // Read port A, in 2bit
    input wire [AW-1:0] addra,
    output reg [DW-1:0] douta,
    // Read port B, in 2bit
    input wire [AW-1:0] addrb,
    output reg [DW-1:0] doutb
);

    wire [RDW-1:0] bram_douta;
    wire [RDW-1:0] bram_doutb;

    mu_ram_2rw #(
        .AW(AW-2),
        .DW(RDW)
    ) mu_ram_2rw (
        .clka(clk),
        .wea(we),
        .addra(we ? addr : addra[AW-1:2]),
        .dina(din),
        .douta(bram_douta),
        .clkb(clk),
        .web(1'b0),
        .addrb(addrb[AW-1:2]),
        .dinb('d0),
        .doutb(bram_doutb)
    );

    integer i;
    always @(*) begin
        douta = 'bx;
        doutb = 'bx;
        for (i = 0; i < 4; i = i + 1) begin
            if (addra[1:0] == i)
                douta = bram_douta[i*DW+:DW];
            if (addrb[1:0] == i)
                doutb = bram_doutb[i*DW+:DW];
        end
    end

endmodule

`default_nettype wire
