`timescale 1ns / 1ps
`default_nettype none
//
// mu_fifo_async.v: Dual Clock Asynchronous FIFO
//
// Copyright 2024 Wenting Zhang <zephray@outlook.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
module mu_fifo_async #(
    parameter DW = 32,        // Memory width
    parameter DEPTH = 4       // FIFO depth
) (  // write port
    input  wire             wr_clk,
    input  wire             wr_nreset,
    input  wire [DW-1:0]    wr_din,        // data to write
    input  wire             wr_valid,      // write fifo
    output wire             wr_ready,      // fifo not full
    // read port
    input  wire             rd_clk,
    input  wire             rd_nreset,
    output wire [DW-1:0]    rd_dout,       // output data (next cycle)
    input  wire             rd_ready,      // read fifo
    output wire             rd_valid       // fifo is not empty
);

    wire rd_empty;
    wire wr_full;

    xpm_fifo_async #(
        .CASCADE_HEIGHT(0),
        .CDC_SYNC_STAGES(2),
        .DOUT_RESET_VALUE("0"),
        .ECC_MODE("no_ecc"),
        .FIFO_MEMORY_TYPE("auto"),
        .FIFO_READ_LATENCY(0),
        .FIFO_WRITE_DEPTH(DEPTH),
        .FULL_RESET_VALUE(0),
        .PROG_EMPTY_THRESH(10),
        .PROG_FULL_THRESH(10),
        .RD_DATA_COUNT_WIDTH(1),
        .READ_DATA_WIDTH(DW),
        .READ_MODE("fwft"),
        .RELATED_CLOCKS(0),
        .SIM_ASSERT_CHK(0),
        .USE_ADV_FEATURES("0000"),
        .WAKEUP_TIME(0),
        .WRITE_DATA_WIDTH(DW),
        .WR_DATA_COUNT_WIDTH(1)
    )
    xpm_fifo_async_inst (
        .almost_empty(),
        .almost_full(),
        .data_valid(),
        .dbiterr(),
        .dout(rd_dout),
        .empty(rd_empty),
        .full(wr_full),
        .overflow(),
        .prog_empty(),
        .prog_full(),
        .rd_data_count(),
        .rd_rst_busy(),
        .sbiterr(),
        .underflow(),
        .wr_ack(),
        .wr_data_count(),
        .wr_rst_busy(),
        .din(wr_din),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rd_clk(rd_clk),
        .rd_en(rd_ready),
        .rst(!wr_nreset),
        .sleep(1'b0),
        .wr_clk(wr_clk),
        .wr_en(wr_valid)
    );

    assign rd_valid = !rd_empty;
    assign wr_ready = !wr_full;

endmodule

`default_nettype wire
