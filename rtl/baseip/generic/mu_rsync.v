`timescale 1ns / 1ps
`default_nettype none
//
// mu_rsync.v: Reset Synchronizer
//
// This file is adapted from the lambdalib project
// Copyright Lambda Project Authors. All rights Reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
module mu_rsync (
    input  wire oclk,
    input  wire in,   // input reset
    output wire out   // synchronized reset
);
    parameter STAGES = 2;
    parameter RND = 1;

    reg     [STAGES:0] sync_pipe;
    integer            sync_delay;

    always @(posedge oclk or posedge in)
        if (in)
            sync_pipe[STAGES:0] <= 'b0;
        else
            sync_pipe[STAGES:0] <= {sync_pipe[STAGES-1:0], 1'b1};

`ifndef SYNTHESIS
    always @(posedge oclk)
        sync_delay <= {$random} % 2;

    assign out = (|sync_delay & (|RND)) ? !sync_pipe[STAGES] : !sync_pipe[STAGES-1];
`else
    assign out = !sync_pipe[STAGES-1];
`endif

endmodule

`default_nettype wire
