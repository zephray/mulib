`timescale 1ns / 1ps
`default_nettype none
//
// mu_widthadapt_4_to_1.v: Width adapter 4:1
// Copyright 2024 Wenting Zhang <zephray@outlook.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
module mu_widthadapt_4_to_1 #(
    parameter IW = 64,
    parameter OW = IW / 4
) (
    input  wire             clk,
    input  wire             rst,
    // Incoming port
    input  wire [IW-1:0]    wr_data,
    input  wire             wr_valid,
    output wire             wr_ready,
    // Outgoing port
    output wire [OW-1:0]    rd_data,
    output wire             rd_valid,
    input  wire             rd_ready
);

    reg [IW-1:0] fifo;
    reg [2:0] fifo_level;
    wire fifo_almost_empty = (fifo_level == 1);
    wire fifo_empty = (fifo_level == 0);

    always @(posedge clk) begin
        if (fifo_empty) begin
            // Output invalid, if with valid input, fill input
            if (wr_valid) begin
                fifo <= wr_data;
                fifo_level <= 4;
            end
        end
        else if (fifo_almost_empty) begin
            // Almost empty, output valid, input ready only if output is ready
            if (rd_ready && wr_valid) begin
                fifo <= wr_data;
                fifo_level <= 4;
            end
            else if (rd_ready) begin
                fifo_level <= 0;
            end
        end
        else begin
            // Output valid, input not ready, if with valid output, shift
            if (rd_ready) begin
                fifo_level <= fifo_level - 1;
            end
        end

        if (rst) begin
            fifo_level <= 0;
        end
    end

    // RX data if fifo is empty
    assign wr_ready = fifo_empty || (fifo_almost_empty && rd_ready);
    assign rd_valid = !fifo_empty;
    assign rd_data =
            (fifo_level == 4) ? fifo[OW*3+:OW] :
            (fifo_level == 3) ? fifo[OW*2+:OW] :
            (fifo_level == 2) ? fifo[OW+:OW] :
            (fifo_level == 1) ? fifo[0+:OW] : {OW{1'bx}};

endmodule

`default_nettype wire
