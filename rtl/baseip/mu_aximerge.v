`timescale 1ns / 1ps
`default_nettype none
`include "mu_defines.vh"
//
// mu_aximerge.v: Merge read-only AXI and write-only AXI into one
//
// Copyright 2024 Wenting Zhang <zephray@outlook.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
module mu_aximerge #(
    parameter AXI_AW = 32,
    parameter AXI_DW = 64,
    parameter AXI_IDW = 1
) (
    /* verilator lint_off UNUSEDSIGNAL */
    input  wire                 clk,    // Clock
    input  wire                 rst,    // Synchronous reset active high

    input  wire [AXI_IDW-1:0]   srd_axi_awid,
    input  wire [AXI_AW-1:0]    srd_axi_awaddr,
    input  wire [7:0]           srd_axi_awlen,
    input  wire [2:0]           srd_axi_awsize,
    input  wire [1:0]           srd_axi_awburst,
    input  wire                 srd_axi_awvalid,
    output wire                 srd_axi_awready,
    input  wire [AXI_DW-1:0]    srd_axi_wdata,
    input  wire [AXI_DW/8-1:0]  srd_axi_wstrb,
    input  wire                 srd_axi_wlast,
    input  wire                 srd_axi_wvalid,
    output wire                 srd_axi_wready,
    output wire [AXI_IDW-1:0]   srd_axi_bid,
    output wire [1:0]           srd_axi_bresp,
    output wire                 srd_axi_bvalid,
    input  wire                 srd_axi_bready,
    input  wire [AXI_IDW-1:0]   srd_axi_arid,
    input  wire [AXI_AW-1:0]    srd_axi_araddr,
    input  wire [7:0]           srd_axi_arlen,
    input  wire [2:0]           srd_axi_arsize,
    input  wire [1:0]           srd_axi_arburst,
    input  wire                 srd_axi_arvalid,
    output wire                 srd_axi_arready,
    output wire [AXI_IDW-1:0]   srd_axi_rid,
    output wire [AXI_DW-1:0]    srd_axi_rdata,
    output wire [1:0]           srd_axi_rresp,
    output wire                 srd_axi_rlast,
    output wire                 srd_axi_rvalid,
    input  wire                 srd_axi_rready,

    input  wire [AXI_IDW-1:0]   swr_axi_awid,
    input  wire [AXI_AW-1:0]    swr_axi_awaddr,
    input  wire [7:0]           swr_axi_awlen,
    input  wire [2:0]           swr_axi_awsize,
    input  wire [1:0]           swr_axi_awburst,
    input  wire                 swr_axi_awvalid,
    output wire                 swr_axi_awready,
    input  wire [AXI_DW-1:0]    swr_axi_wdata,
    input  wire [AXI_DW/8-1:0]  swr_axi_wstrb,
    input  wire                 swr_axi_wlast,
    input  wire                 swr_axi_wvalid,
    output wire                 swr_axi_wready,
    output wire [AXI_IDW-1:0]   swr_axi_bid,
    output wire [1:0]           swr_axi_bresp,
    output wire                 swr_axi_bvalid,
    input  wire                 swr_axi_bready,
    input  wire [AXI_IDW-1:0]   swr_axi_arid,
    input  wire [AXI_AW-1:0]    swr_axi_araddr,
    input  wire [7:0]           swr_axi_arlen,
    input  wire [2:0]           swr_axi_arsize,
    input  wire [1:0]           swr_axi_arburst,
    input  wire                 swr_axi_arvalid,
    output wire                 swr_axi_arready,
    output wire [AXI_IDW-1:0]   swr_axi_rid,
    output wire [AXI_DW-1:0]    swr_axi_rdata,
    output wire [1:0]           swr_axi_rresp,
    output wire                 swr_axi_rlast,
    output wire                 swr_axi_rvalid,
    input  wire                 swr_axi_rready,

    output wire [AXI_IDW-1:0]   m_axi_awid,
    output wire [AXI_AW-1:0]    m_axi_awaddr,
    output wire [7:0]           m_axi_awlen,
    output wire [2:0]           m_axi_awsize,
    output wire [1:0]           m_axi_awburst,
    output wire                 m_axi_awvalid,
    input  wire                 m_axi_awready,
    output wire [AXI_DW-1:0]    m_axi_wdata,
    output wire [AXI_DW/8-1:0]  m_axi_wstrb,
    output wire                 m_axi_wlast,
    output wire                 m_axi_wvalid,
    input  wire                 m_axi_wready,
    input  wire [AXI_IDW-1:0]   m_axi_bid,
    input  wire [1:0]           m_axi_bresp,
    input  wire                 m_axi_bvalid,
    output wire                 m_axi_bready,
    output wire [AXI_IDW-1:0]   m_axi_arid,
    output wire [AXI_AW-1:0]    m_axi_araddr,
    output wire [7:0]           m_axi_arlen,
    output wire [2:0]           m_axi_arsize,
    output wire [1:0]           m_axi_arburst,
    output wire                 m_axi_arvalid,
    input  wire                 m_axi_arready,
    input  wire [AXI_IDW-1:0]   m_axi_rid,
    input  wire [AXI_DW-1:0]    m_axi_rdata,
    input  wire [1:0]           m_axi_rresp,
    input  wire                 m_axi_rlast,
    input  wire                 m_axi_rvalid,
    output wire                 m_axi_rready
    /* verilator lint_on UNUSEDSIGNAL */
);

    // RD
    assign m_axi_arid = srd_axi_arid;
    assign m_axi_araddr = srd_axi_araddr;
    assign m_axi_arlen = srd_axi_arlen;
    assign m_axi_arsize = srd_axi_arsize;
    assign m_axi_arburst = srd_axi_arburst;
    assign m_axi_arvalid = srd_axi_arvalid;
    assign srd_axi_arready = m_axi_arready;
    assign srd_axi_rid = m_axi_rid;
    assign srd_axi_rdata = m_axi_rdata;
    assign srd_axi_rresp = m_axi_rresp;
    assign srd_axi_rlast = m_axi_rlast;
    assign srd_axi_rvalid = m_axi_rvalid;
    assign m_axi_rready = srd_axi_rready;

    // WR
    assign m_axi_awid = swr_axi_awid;
    assign m_axi_awaddr = swr_axi_awaddr;
    assign m_axi_awlen = swr_axi_awlen;
    assign m_axi_awsize = swr_axi_awsize;
    assign m_axi_awburst = swr_axi_awburst;
    assign m_axi_awvalid = swr_axi_awvalid;
    assign swr_axi_awready = m_axi_awready;
    assign m_axi_wdata = swr_axi_wdata;
    assign m_axi_wstrb = swr_axi_wstrb;
    assign m_axi_wlast = swr_axi_wlast;
    assign m_axi_wvalid = swr_axi_wvalid;
    assign swr_axi_wready = m_axi_wready;
    assign swr_axi_bid = m_axi_bid;
    assign swr_axi_bresp = m_axi_bresp;
    assign swr_axi_bvalid = m_axi_bvalid;
    assign m_axi_bready = swr_axi_bready;

    // TIEOFF
    assign srd_axi_awready = 1'b0;
    assign srd_axi_wready = 1'b0;
    assign srd_axi_bid = 'd0;
    assign srd_axi_bresp = 'd0;
    assign srd_axi_bvalid = 1'b0;

    assign swr_axi_arready = 1'b0;
    assign swr_axi_rid = 'd0;
    assign swr_axi_rdata = 'd0;
    assign swr_axi_rresp = 'd0;
    assign swr_axi_rlast = 1'b0;
    assign swr_axi_rvalid = 1'b0;

endmodule
