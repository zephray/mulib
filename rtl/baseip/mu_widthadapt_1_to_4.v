`timescale 1ns / 1ps
`default_nettype none
//
// mu_widthadapt_1_to_4.v: Width adapter 1:4
// Copyright 2024 Wenting Zhang <zephray@outlook.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
module mu_widthadapt_1_to_4 #(
    parameter IW = 16,
    parameter OW = IW * 4,
    parameter ORDER = 0
) (
    input  wire             clk,
    input  wire             rst,
    // Incoming port
    input  wire [IW-1:0]    wr_data,
    input  wire             wr_valid,
    output wire             wr_ready,
    // Outgoing port
    output wire [OW-1:0]    rd_data,
    output wire             rd_valid,
    input  wire             rd_ready
);

    // Store from higher order byte to lower order
    reg [OW-1:0] fifo;
    reg [2:0] fifo_level;
    wire fifo_full = (fifo_level == 4);

    wire [OW-1:0] new_data =
            (fifo_level == 3) ? {fifo[OW-1:IW], wr_data} :
            (fifo_level == 2) ? {fifo[OW-1:IW*2], wr_data, {IW{1'b0}}} :
            (fifo_level == 1) ? {fifo[OW-1:IW*3], wr_data, {(IW*2){1'b0}}} :
            ((fifo_level == 0) || (fifo_level == 4)) ? {wr_data, {(IW*3){1'b0}}} :
            {OW{1'bx}};

    always @(posedge clk) begin
        if (fifo_full) begin
            if (rd_ready && wr_valid) begin
                // Output ready, input valid, shiftin new data
                fifo <= new_data;
                fifo_level <= 1;
            end
            else if (rd_ready) begin
                // Output ready, no input, output data
                fifo_level <= 0;
            end
        end
        else begin
            // Not full, output not valid, input ready
            if (wr_valid) begin
                fifo <= new_data;
                fifo_level <= fifo_level + 1;
            end
        end

        if (rst) begin
            fifo_level <= 0;
        end
    end

    // RX data if fifo is empty
    assign wr_ready = !fifo_full || (fifo_full && rd_ready);
    assign rd_valid = fifo_full;
    assign rd_data = (ORDER == 0) ? fifo :
        {fifo[0+:IW], fifo[IW+:IW], fifo[IW*2+:IW], fifo[IW*3+:IW]};

endmodule

`default_nettype wire
