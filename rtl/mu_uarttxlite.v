`timescale 1ns / 1ps
`default_nettype none
`include "mu_defines.vh"
//
// mu_uarttxlite.v: Very basic transmit-only UART
//
// Copyright 2025 Wenting Zhang <zephray@outlook.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
module mu_uarttxlite #(
    // 100MHz 115200 / 50MHz 57600: 868 - 1
    // 100MHz 230400 / 50MHz 115200: 434 - 1
    // 50MHz 230400 / 25MHz 115200: 217 - 1
    parameter UART_DIV = 10'd216
) (
    input wire clk,
    input wire rst,
    input wire valid,
    output reg ready,
    input wire [7:0] dat,
    output wire txd
);

    reg [9:0] counter;
    reg [3:0] tx_count;
    reg [10:0] shift_reg;
    always@(posedge clk) begin
        if (counter != 10'd0) begin
            counter <= counter - 1;
            if (ready && valid) begin
                            // Stop, Data, Start, Idle
                shift_reg <= {1'b1, dat, 1'b0, 1'b1};
                tx_count <= 4'd11;
                ready <= 1'b0;
            end
        end
        else begin
            shift_reg <= {1'b1, shift_reg[10:1]};
            counter <= UART_DIV;
            if (tx_count != 0) begin
                tx_count <= tx_count - 1;
            end
            else if (tx_count == 'd0) begin
                ready <= 1'b1;
            end
        end

        if (rst) begin
            counter <= UART_DIV;
            tx_count <= 0;
            ready <= 1'b0;
            shift_reg <= 11'b11111111111;
        end    
    end
    
    assign txd = shift_reg[0];

endmodule
