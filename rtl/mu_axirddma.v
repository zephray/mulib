`timescale 1ns / 1ps
`default_nettype none
`include "mu_defines.vh"
//
// mu_axirddma.v: Reusable AXI read-only DMA module
//
// Copyright 2024 Wenting Zhang <zephray@outlook.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
module mu_axirddma #(
    parameter AXI_AW = 32,
    parameter AXI_DW = 64
) (
    input  wire                 clk,    // Clock
    input  wire                 rst,    // Synchronous reset active high
    // AXI master port
    /* verilator lint_off UNUSEDSIGNAL */
    `AXI_MASTER_IF(AXI_AW, AXI_DW, 1),  // Doesn't really need ID
    /* verilator lint_on UNUSEDSIGNAL */
    // Data output
    output wire [AXI_DW-1:0]    rd_dout,
    output wire                 rd_valid,
    input  wire                 rd_ready,
    // Control
    input  wire [AXI_AW-1:0]    ctl_startaddr,
    input  wire [AXI_AW-1:0]    ctl_endaddr,
    input  wire [7:0]           ctl_burstlen,
    input  wire [7:0]           ctl_maxinflight,
    input  wire                 ctl_trigger,
    output reg                  ctl_busy,
    output reg                  ctl_error
);

    localparam ARSIZE = $clog2(AXI_DW / 8); // Pick data bus width

    // Tieoff AXI AW/W/B channel, not used
    assign m_axi_awid = 'd0;
    assign m_axi_awaddr = 'd0;
    assign m_axi_awlen = 'd0;
    assign m_axi_awsize = 'd0;
    assign m_axi_awburst = 'd0;
    assign m_axi_awvalid = 'd0;
    assign m_axi_wdata = 'd0;
    assign m_axi_wstrb = 'd0;
    assign m_axi_wlast = 'd0;
    assign m_axi_wvalid = 'd0;
    assign m_axi_bready = 'd0;

    reg [AXI_AW-1:0] araddr;
    reg arvalid;

    assign m_axi_arid = 'd0; // This module doesn't use ID
    assign m_axi_araddr = araddr;
    assign m_axi_arlen = ctl_burstlen - 'd1;
    assign m_axi_arsize = ARSIZE[2:0];
    assign m_axi_arburst = `AXI_BURST_INCR;
    assign m_axi_arvalid = arvalid;

    assign rd_dout = m_axi_rdata;
    assign rd_valid = m_axi_rvalid;
    assign m_axi_rready = rd_ready;

    reg [7:0] inflight;

    wire arhandshake = m_axi_arvalid && m_axi_arready;
    wire rburstfinish = m_axi_rvalid && m_axi_rready && m_axi_rlast;
    /* verilator lint_off WIDTHEXPAND */
    wire [AXI_AW-1:0] araddr_next = araddr + {ctl_burstlen, {ARSIZE{1'b0}}};
    /* verilator lint_on WIDTHEXPAND */
    wire next_reached = araddr_next == ctl_endaddr;
    reg reachend_end;
    
    always @(posedge clk) begin
        if (!ctl_busy) begin
            // Not currently active
            if (ctl_trigger) begin
               inflight <= 'd0;
               araddr <= ctl_startaddr;
               arvalid <= 1'b1;
               ctl_busy <= 1'b1;
               ctl_error <= 1'b0; 
               reachend_end <= 1'b0;
            end
        end
        else begin
            // Currently active

            // Finish handshake
            if (arhandshake) begin
                arvalid <= 1'b0;
            end

            // Generate new request
            if (!arvalid && !reachend_end && (inflight < ctl_maxinflight)) begin
                araddr <= araddr_next;
                if (next_reached)
                    reachend_end <= 1'b1;
                else
                    arvalid <= 1'b1;
            end

            // Update inflight counter
            if (arhandshake && !rburstfinish) begin
                inflight <= inflight + 'd1;
            end
            else if (!arhandshake && rburstfinish) begin
                inflight <= inflight - 'd1;
            end

            // Check accepted response
            if (rburstfinish) begin
                if (m_axi_rresp != `AXI_RESP_OKAY) begin
                    ctl_error <= 1'b1;
                end
            end

            // Check end condition
            if (reachend_end && (inflight == 'd0)) begin
                ctl_busy <= 1'b0;
            end
        end

        if (rst) begin
            araddr <= 'd0;
            arvalid <= 1'b0;
            ctl_busy <= 1'b0;
            ctl_error <= 1'b0;
        end
    end

endmodule

`default_nettype wire
