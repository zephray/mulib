`timescale 1ns / 1ps
`default_nettype none
`include "mu_defines.vh"
//
// mu_ddif.v: Digital display interface controller
//
// Copyright 2024 Wenting Zhang <zephray@outlook.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
module mu_ddif #(
    parameter AXI_AW = 32,
    parameter AXI_DW = 64   // This module only works with 64-bit wide DW
) (
    input  wire         clk,        // Clock
    input  wire         clk_pix,    // Pixel clock
    input  wire         rst,
    input  wire         rst_pix,
	// APB device port for register access
    `APB_SLAVE_IF,
	// AXI host port for direct memory access
    `AXI_MASTER_IF(AXI_AW, AXI_DW, 1), // Doesn't really need ID
	// Display interface
    input  wire         dpi_ext_vsync,
    output wire         dpi_pixclk,
    output reg          dpi_vsync,
    output reg          dpi_hsync,
    output reg          dpi_enable,
    output reg [23:0]   dpi_data
);

    localparam DDIF_REG_PCTL        = 16'h0;
    localparam DDIF_REG_DMACTL      = 16'h4;
    localparam DDIF_REG_VPORCH      = 16'h10;
    localparam DDIF_REG_VACT        = 16'h14;
    localparam DDIF_REG_HPORCH      = 16'h18;
    localparam DDIF_REG_HACT        = 16'h1C;
    localparam DDIF_REG_STARTADDR_L = 16'h20;
    localparam DDIF_REG_STARTADDR_H = 16'h24;
    localparam DDIF_REG_ENDADDR_L   = 16'h28;
    localparam DDIF_REG_ENDADDR_H   = 16'h2C;

    /* verilator lint_off UNUSEDSIGNAL */
    reg [19:0] ddif_pctl;
    reg [15:0] ddif_dmactl;
    reg [23:0] ddif_vporch;
    reg [11:0] ddif_vact;
    reg [23:0] ddif_hporch;
    reg [11:0] ddif_hact;
    reg [31:0] ddif_startaddr_l;
    reg [31:0] ddif_startaddr_h;
    reg [31:0] ddif_endaddr_l;
    reg [31:0] ddif_endaddr_h;
    /* verilator lint_on UNUSEDSIGNAL */

    // APB Access
    always @(posedge clk) begin
        if (s_apb_penable && s_apb_pwrite && s_apb_psel) begin
            case (s_apb_paddr[15:0])
            DDIF_REG_PCTL: ddif_pctl <= s_apb_pwdata[19:0];
            DDIF_REG_DMACTL: ddif_dmactl <= s_apb_pwdata[15:0];
            DDIF_REG_VPORCH: ddif_vporch <= s_apb_pwdata[23:0];
            DDIF_REG_VACT: ddif_vact <= s_apb_pwdata[11:0];
            DDIF_REG_HPORCH: ddif_hporch <= s_apb_pwdata[23:0];
            DDIF_REG_HACT: ddif_hact <= s_apb_pwdata[11:0];
            DDIF_REG_STARTADDR_L: ddif_startaddr_l <= s_apb_pwdata;
            DDIF_REG_STARTADDR_H: ddif_startaddr_h <= s_apb_pwdata;
            DDIF_REG_ENDADDR_L: ddif_endaddr_l <= s_apb_pwdata;
            DDIF_REG_ENDADDR_H: ddif_endaddr_h <= s_apb_pwdata;
            default: ; // nothing
            endcase
        end

        if (rst) begin
            ddif_pctl <= 20'd0;
            ddif_dmactl <= 16'd0;
        end
    end

    assign s_apb_pready = 1'b1;
    reg [31:0] reg_rd;
    always @(*) begin
        case (s_apb_paddr[15:0])
        DDIF_REG_PCTL: reg_rd = {12'd0, ddif_pctl};
        DDIF_REG_DMACTL: reg_rd = {16'd0, ddif_dmactl};
        DDIF_REG_VPORCH: reg_rd = {8'd0, ddif_vporch};
        DDIF_REG_VACT: reg_rd = {20'd0, ddif_vact};
        DDIF_REG_HPORCH: reg_rd = {8'd0, ddif_hporch};
        DDIF_REG_HACT: reg_rd = {20'd0, ddif_hact};
        DDIF_REG_STARTADDR_L: reg_rd = ddif_startaddr_l;
        DDIF_REG_STARTADDR_H: reg_rd = ddif_startaddr_h;
        DDIF_REG_ENDADDR_L: reg_rd = ddif_endaddr_l;
        DDIF_REG_ENDADDR_H: reg_rd = ddif_endaddr_h;
        default: reg_rd = 32'hdeadbeef;
        endcase
    end
    assign s_apb_prdata = reg_rd;

    // Register fields
    // Fields used in clk domain
    wire tgen_en_apb_clock = ddif_pctl[0];
    wire dc_swap_32b = ddif_pctl[2]; // 0 - low 32b first, 1 - high 32b first
    wire dc_swap_rgbx = ddif_pctl[3]; // 0 - xrgb, 1 - rgbx
    wire dc_swap_bgr = ddif_pctl[4]; // 0 - rgb, 1 - bgr
    wire [7:0] ctl_burstlen = ddif_dmactl[7:0];
    wire [7:0] ctl_maxinflight = ddif_dmactl[15:8];

    // Fields used in clk_pix domain
    wire [19:0] ddif_pctl_clk_pix;
    wire [23:0] ddif_vporch_clk_pix;
    wire [11:0] ddif_vact_clk_pix;
    wire [23:0] ddif_hporch_clk_pix;
    wire [11:0] ddif_hact_clk_pix;
    mu_dbsync #(20) pctl_sync (clk_pix, epdif_pctl, epdif_pctl_clk_pix);
    mu_dbsync #(24) vporch_sync (clk_pix, epdif_vporch, epdif_vporch_clk_pix);
    mu_dbsync #(12) vact_sync (clk_pix, epdif_vact, epdif_vact_clk_pix);
    mu_dbsync #(24) hporch_sync (clk_pix, epdif_hporch, epdif_hporch_clk_pix);
    mu_dbsync #(12) hact_sync (clk_pix, epdif_hact, epdif_hact_clk_pix);

    wire tgen_en = ddif_pctl_clk_pix[0];
    wire tgen_use_ext_vsync = ddif_pctl_clk_pix[1];
    wire [7:0] tgen_ext_vsync_delay = ddif_pctl_clk_pix[15:8];
    wire tgen_vsync_pol = ddif_pctl_clk_pix[16];
    wire tgen_hsync_pol = ddif_pctl_clk_pix[17];
    wire tgen_den_pol = ddif_pctl_clk_pix[18];
    wire tgen_pclk_pol = ddif_pctl_clk_pix[19];

    wire [7:0] vfp = ddif_vporch_clk_pix[7:0];
    wire [7:0] vsync = ddif_vporch_clk_pix[15:8];
    wire [7:0] vbp = ddif_vporch_clk_pix[23:16];
    wire [11:0] vact = ddif_vact_clk_pix[11:0];
    wire [7:0] hfp = ddif_hporch_clk_pix[7:0];
    wire [7:0] hsync = ddif_hporch_clk_pix[15:8];
    wire [7:0] hbp = ddif_hporch_clk_pix[23:16];
    wire [11:0] hact = ddif_hact_clk_pix[11:0];

    // Timing Gen
    /* verilator lint_off WIDTHEXPAND */
    wire [12:0] vtotal = vfp + vsync + vbp + vact;
    wire [12:0] htotal = hfp + hsync + hbp + hact;
    /* verilator lint_on WIDTHEXPAND */

    reg [12:0] scan_v_cnt;
    reg [12:0] scan_h_cnt;

    reg [1:0] scan_state;
    localparam SCAN_IDLE = 2'd0;
    localparam SCAN_WAITING = 2'd1;
    localparam SCAN_RUNNING = 2'd2;

    always @(posedge clk_pix) begin
        case (scan_state)
        SCAN_IDLE: begin
            if (tgen_en && (!tgen_use_ext_vsync || dpi_ext_vsync)) begin
                scan_state <= SCAN_WAITING;
                ctl_trigger <= 1'b1;
            end
            scan_h_cnt <= 0;
            scan_v_cnt <= 0;
        end
        SCAN_WAITING: begin
            if (scan_h_cnt == {5'd0, tgen_ext_vsync_delay}) begin
                scan_state <= SCAN_RUNNING;
                scan_h_cnt <= 0;
            end
            else begin
                scan_h_cnt <= scan_h_cnt + 1;
            end
            ctl_trigger <= 1'b0;
        end
        SCAN_RUNNING: begin
            ctl_trigger <= 1'b0;
            if (scan_h_cnt == htotal - 1) begin
                if (scan_v_cnt == vtotal - 1) begin
                    if (tgen_use_ext_vsync) begin
                        scan_state <= SCAN_IDLE;
                    end
                    else begin
                        // Immediately restart if external Vsync is not used
                        ctl_trigger <= 1'b1;
                        scan_h_cnt <= 'd0;
                        scan_v_cnt <= 'd0;
                    end
                end
                else begin
                    scan_h_cnt <= 0;
                    scan_v_cnt <= scan_v_cnt + 1;
                end
            end
            else begin
                scan_h_cnt <= scan_h_cnt + 1;
            end
        end
        default: begin
            // Invalid state
            $display("Scan FSM in invalid state");
            scan_state <= SCAN_IDLE;
        end
        endcase

        if (rst) begin
            scan_state <= SCAN_IDLE;
            scan_h_cnt <= 0;
            scan_v_cnt <= 0;
        end
    end

    /* verilator lint_off width */
    wire scan_in_vsync = (scan_state != SCAN_IDLE) &&
        (scan_v_cnt >= vfp) &&
        (scan_v_cnt < (vfp + vsync));
    wire scan_in_vbp = (scan_state != SCAN_IDLE) &&
        (scan_v_cnt >= (vfp + vsync)) &&
        (scan_v_cnt < (vfp + vsync + vbp));
    wire scan_in_vact = (scan_state != SCAN_IDLE) &&
        (scan_v_cnt >= (vfp + vsync + vbp));

    wire scan_in_hfp = (scan_state != SCAN_IDLE) &&
        (scan_h_cnt < hfp);
    wire scan_in_hsync = (scan_state != SCAN_IDLE) &&
        (scan_h_cnt >= hfp) &&
        (scan_h_cnt < (hfp + hsync));
    wire scan_in_hbp = (scan_state != SCAN_IDLE) &&
        (scan_h_cnt >= (hfp + hsync)) &&
        (scan_h_cnt < (hfp + hsync + hbp));
    wire scan_in_hact = (scan_state != SCAN_IDLE) &&
        (scan_h_cnt >= (hfp + hsync + hbp));
    /* verilator lint_on width */

    wire scan_in_act = scan_in_vact && scan_in_hact;

    // DMA Data output
    wire [AXI_DW-1:0] dmard_dout;
    wire dmard_valid;
    wire dmard_ready;
    // DMA Control
    wire [AXI_AW-1:0] ctl_startaddr;
    wire [AXI_AW-1:0] ctl_endaddr;
    generate
        if (AXI_AW > 32) begin: gen_addr_64
            assign ctl_startaddr =
                {ddif_startaddr_h[AXI_AW-32-1:0], ddif_startaddr_l};
            assign ctl_endaddr =
                {ddif_endaddr_h[AXI_AW-32-1:0], ddif_endaddr_l};
        end
        else begin: gen_addr_32
            assign ctl_startaddr = ddif_startaddr_l[AXI_AW-1:0];
            assign ctl_endaddr = ddif_endaddr_l[AXI_AW-1:0];
        end
    endgenerate

    reg ctl_trigger; // internal DMA trigger
    wire ctl_busy;
    wire ctl_error;

    wire ctl_trigger_apb_clk;
    mu_dsync trigger_sync (clk, ctl_trigger, ctl_trigger_apb_clk);

    wire rddma_rstn = tgen_en_apb_clock;

    mu_axirddma mu_axirddma (
        .clk(clk),
        .rst(rst),
        `AXI_CONN(m_, m_),
        // Data output
        .rd_dout(dmard_dout),
        .rd_valid(dmard_valid),
        .rd_ready(dmard_ready),
        // Control
        .ctl_startaddr(ctl_startaddr),
        .ctl_endaddr(ctl_endaddr),
        .ctl_burstlen(ctl_burstlen),
        .ctl_maxinflight(ctl_maxinflight),
        .ctl_trigger(ctl_trigger_apb_clk),
        .ctl_busy(ctl_busy),
        .ctl_error(ctl_error)
    ); 

    wire [23:0] p1h = dc_swap_rgbx ? dmard_dout[63:40] : dmard_dout[55:32];
    wire [23:0] p1l = dc_swap_rgbx ? dmard_dout[31:8] : dmard_dout[23:0];
    wire [23:0] p2h = dc_swap_bgr ? {p1h[7:0], p1h[15:8], p1h[23:16]} : p1h;
    wire [23:0] p2l = dc_swap_bgr ? {p1l[7:0], p1l[15:8], p1l[23:16]} : p1l;
    wire [47:0] dmard_pix = dc_swap_32b ? {p2h, p2l} : {p2l, p2h};

    wire [23:0] dmapix_data;
    wire dmapix_valid;
    wire dmapix_ready;
    mu_widthadapt_2_to_1 #(
        .IW(48),
        .OW(24)
    ) pix_width_adapt (
        .clk(clk),
        .rst(rst),
        // Incoming port
        .wr_data(dmard_pix),
        .wr_valid(dmard_valid),
        .wr_ready(dmard_ready),
        // Outgoing port
        .rd_data(dmapix_data),
        .rd_valid(dmapix_valid),
        .rd_ready(dmapix_ready)
    );

    wire [23:0] scan_pixel;

    wire scan_fifo_valid;
    mu_fifo_async #(.DW(24), .DEPTH(512)) pixel_fifo (
        .wr_clk(clk),
        .wr_nreset(!rst),
        .wr_din(dmapix_data),
        .wr_valid(dmapix_valid),
        .wr_ready(dmapix_ready),
        .wr_full(),
        .rd_clk(clk_pix),
        .rd_nreset(!rst_pix),
        .rd_dout(scan_pixel),
        .rd_ready(scan_in_act),
        .rd_valid(scan_fifo_valid)
    );

    // Signal output
    always @(posedge clk_pix) begin
        dpi_vsync <= scan_in_vsync ^ tgen_vsync_pol;
        dpi_hsync <= scan_in_hsync ^ tgen_hsync_pol;
        dpi_enable <= scan_in_act ^ tgen_den_pol;
        dpi_data <= scan_pixel;
    end
    assign dpi_pixclk = clk_pix ^ tgen_pclk_pol;

endmodule

`default_nettype wire
