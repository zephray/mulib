`timescale 1ns / 1ps
`default_nettype none
`include "mu_defines.vh"
//
// mu_dphy_7series.v: MIPI D-PHY implementation on 7-series
//
// Copyright 2024 Wenting Zhang <zephray@outlook.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
module mu_dphy_7series #(
    parameter LANES = 2
) (
    input  wire                 clk,
    input  wire                 rst,
    output wire                 locked,
    output wire                 clk_lcd,
    output wire                 rst_lcd,
    input  wire                 hsck_ten,
    input  wire                 hsdat_ten,
    input  wire [LANES*8-1:0]   dat,
    output wire                 dsi_hs_cp,
    output wire                 dsi_hs_cn,
    output wire [LANES-1:0]     dsi_hs_dp,
    output wire [LANES-1:0]     dsi_hs_dn
);

    // Clock multiplier
    wire ddr_bit_clk_serdes;
    wire ddr_bit_clk_output;

    wire clkfbout;
    wire clkout0;
    wire clkout1;
    wire clkout2;
    wire clkfbout_buf;
    wire mmcm_locked;
    MMCME2_ADV #(
        .BANDWIDTH            ("OPTIMIZED"),
        .CLKOUT4_CASCADE      ("FALSE"),
        .COMPENSATION         ("ZHOLD"),
        .STARTUP_WAIT         ("FALSE"),
        .DIVCLK_DIVIDE        (1),
        .CLKFBOUT_MULT_F      (20.000), // 1GHz
        .CLKFBOUT_PHASE       (0.000),
        .CLKFBOUT_USE_FINE_PS ("FALSE"),
        .CLKOUT0_DIVIDE_F     (8.000),  // 125MHz
        .CLKOUT0_PHASE        (0.000),
        .CLKOUT0_DUTY_CYCLE   (0.500),
        .CLKOUT0_USE_FINE_PS  ("FALSE"),
        .CLKOUT1_DIVIDE       (8.000),  // 125MHz
        .CLKOUT1_PHASE        (90.000),
        .CLKOUT1_DUTY_CYCLE   (0.500),
        .CLKOUT1_USE_FINE_PS  ("FALSE"),
        .CLKIN1_PERIOD        (20.00)
    ) mmcm_adv_inst (
        // Output clocks
        .CLKFBOUT            (clkfbout),
        .CLKFBOUTB           (),
        .CLKOUT0             (clkout0),
        .CLKOUT0B            (),
        .CLKOUT1             (clkout1),
        .CLKOUT1B            (),
        .CLKOUT2             (),
        .CLKOUT2B            (),
        .CLKOUT3             (),
        .CLKOUT3B            (),
        .CLKOUT4             (),
        .CLKOUT5             (),
        .CLKOUT6             (),
        // Input clock control
        .CLKFBIN             (clkfbout_buf),
        .CLKIN1              (clk),
        .CLKIN2              (1'b0),
        // Tied to always select the primary input clock
        .CLKINSEL            (1'b1),
        // Ports for dynamic reconfiguration
        .DADDR               (7'h0),
        .DCLK                (1'b0),
        .DEN                 (1'b0),
        .DI                  (16'h0),
        .DO                  (),
        .DRDY                (),
        .DWE                 (1'b0),
        // Ports for dynamic phase shift
        .PSCLK               (1'b0),
        .PSEN                (1'b0),
        .PSINCDEC            (1'b0),
        .PSDONE              (),
        // Other control and status signals
        .LOCKED              (mmcm_locked),
        .CLKINSTOPPED        (),
        .CLKFBSTOPPED        (),
        .PWRDWN              (1'b0),
        .RST                 (rst)
    );

    assign locked = mmcm_locked;

    BUFG clkfb_buf (
        .I(clkfbout),
        .O(clkfbout_buf)
    );

    BUFIO clkout0_buf (
        .I(clkout0),
        .O(ddr_bit_clk_output)
    );

    BUFIO clkout1_buf (
        .I(clkout1),
        .O(ddr_bit_clk_serdes)
    );

    wire fwd_clk_div;
    BUFR #(
        .BUFR_DIVIDE(4)
    ) clkout0_bufr (
        .I(clkout0),
        .O(fwd_clk_div)
    );

    BUFR #(
        .BUFR_DIVIDE(4)
    ) clkout1_bufr (
        .I(clkout1),
        .O(clk_lcd)
    );
    
    wire fwd_clk_rst;

    xpm_cdc_sync_rst fwd_rst_sync(
        .dest_clk(fwd_clk_div),
        .src_rst(!mmcm_locked),
        .dest_rst(fwd_clk_rst)
    );

    xpm_cdc_sync_rst lcd_rst_sync(
        .dest_clk(clk_lcd),
        .src_rst(!mmcm_locked),
        .dest_rst(rst_lcd)
    );

    // Clock output
    wire clk_q;
    wire clk_tq;
    // Data output
    OSERDESE2 #(
        .DATA_WIDTH(8),
        .TRISTATE_WIDTH(1),
        .DATA_RATE_OQ("DDR"),
        .DATA_RATE_TQ("SDR"),
        .SERDES_MODE("MASTER"),
        .SRVAL_OQ(0),
        .SRVAL_TQ(1)
    ) data_serdes (
        .OQ(clk_q),
        .OCE(1'b1),
        .CLK(ddr_bit_clk_output),
        .RST(fwd_clk_rst),
        .CLKDIV(fwd_clk_div),
        .D8(1'b1),
        .D7(1'b0),
        .D6(1'b1),
        .D5(1'b0),
        .D4(1'b1),
        .D3(1'b0),
        .D2(1'b1),
        .D1(1'b0),
        .TQ(clk_tq),
        .T1(hsck_ten),
        .T2(hsck_ten),
        .T3(hsck_ten),
        .T4(hsck_ten),
        .TCE(1'b1),
        .TBYTEIN(1'b0),
        .TBYTEOUT(),
        .OFB(),
        .TFB(),
        .SHIFTOUT1(),
        .SHIFTOUT2(),
        .SHIFTIN1(1'b0),
        .SHIFTIN2(1'b0)
    );

    OBUFTDS obuf_clk (
        .O(dsi_hs_cp),
        .OB(dsi_hs_cn),
        .I(clk_q),
        .T(clk_tq)
    );

    genvar i;
    generate for (i = 0; i < LANES; i = i + 1) begin
        wire dat_q;
        wire dat_tq;
        // Data output
        OSERDESE2 #(
            .DATA_WIDTH(8),
            .TRISTATE_WIDTH(1),
            .DATA_RATE_OQ("DDR"),
            .DATA_RATE_TQ("SDR"),
            .SERDES_MODE("MASTER"),
            .SRVAL_OQ(0),
            .SRVAL_TQ(1)
        ) data_serdes (
            .OQ(dat_q),
            .OCE(1'b1),
            .CLK(ddr_bit_clk_serdes),
            .RST(rst_lcd),
            .CLKDIV(clk_lcd),
            .D8(dat[i*8+7]),
            .D7(dat[i*8+6]),
            .D6(dat[i*8+5]),
            .D5(dat[i*8+4]),
            .D4(dat[i*8+3]),
            .D3(dat[i*8+2]),
            .D2(dat[i*8+1]),
            .D1(dat[i*8+0]),
            .TQ(dat_tq),
            .T1(hsdat_ten),
            .T2(hsdat_ten),
            .T3(hsdat_ten),
            .T4(hsdat_ten),
            .TCE(1'b1),
            .TBYTEIN(1'b0),
            .TBYTEOUT(),
            .OFB(),
            .TFB(),
            .SHIFTOUT1(),
            .SHIFTOUT2(),
            .SHIFTIN1(1'b0),
            .SHIFTIN2(1'b0)
        );

        OBUFTDS obuf_dat (
            .O(dsi_hs_dp[i]),
            .OB(dsi_hs_dn[i]),
            .I(dat_q),
            .T(dat_tq)
        );
    end endgenerate

endmodule
