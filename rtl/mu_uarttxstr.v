`default_nettype none
`timescale 1ns / 1ps
//
// mu_uarttxstr.v: Keep sending specified string
//
// Copyright 2025 Wenting Zhang <zephray@outlook.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
module mu_uarttxstr #(
    parameter UART_DIV = 10'd216,
    parameter STRING = "Hello\n",
    parameter LENGTH = 6
) (
    input wire clk,
    input wire rst,
    output wire txd
);

    reg [9:0] tx_cntr;
    reg [7:0] char;
    wire uart_ready;
    always @(posedge clk) begin
        if (uart_ready) begin
            char <= STRING[tx_cntr*8+:8];
            tx_cntr <= tx_cntr - 'd1;
            if (tx_cntr == 'd0)
                tx_cntr <= LENGTH - 1;
        end

        if (rst) begin
            tx_cntr <= LENGTH - 2;
            char <= STRING[(LENGTH-1)*8+:8];
        end
    end

    mu_uarttxlite #(
        .UART_DIV(UART_DIV)
    ) uart_tx (
        .clk(clk),
        .rst(rst),
        .valid(1'b1),
        .ready(uart_ready),
        .dat(char),
        .txd(txd)
    );

endmodule
