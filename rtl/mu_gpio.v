`timescale 1ns / 1ps
`default_nettype none
`include "mu_defines.vh"
//
// mu_sspi.v: Synchronous serial protocol interface controller
//
// Copyright 2024 Wenting Zhang <zephray@outlook.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
module mu_gpio (
    input  wire         clk,    // Clock
    input  wire         rst,    // Synchronous reset active high
    // APB device port for register access
    /* verilator lint_off UNUSEDSIGNAL */
    `APB_SLAVE_IF,
    /* verilator lint_on UNUSEDSIGNAL */
    // GPIO
    output wire [31:0]  gpio_out,
    input  wire [31:0]  gpio_in,
    output wire [31:0]  gpio_oe
);

    localparam GPIO_REG_ODR     = 16'h0;
    localparam GPIO_REG_IDR     = 16'h4;
    localparam GPIO_REG_BSR     = 16'h8;
    localparam GPIO_REG_BCR     = 16'hC;
    localparam GPIO_REG_OER     = 16'h10;
    localparam GPIO_REG_OESR    = 16'h14;
    localparam GPIO_REG_OECR    = 16'h18;

    reg [31:0] gpio_odr;
    reg [31:0] gpio_idr;
    reg [31:0] gpio_oer;

    // APB access
    always @(posedge clk) begin
        if (s_apb_penable && s_apb_pwrite && s_apb_psel) begin
            case (s_apb_paddr[15:0])
            GPIO_REG_ODR: gpio_odr <= s_apb_pwdata;
            GPIO_REG_BSR: gpio_odr <= gpio_odr | s_apb_pwdata;
            GPIO_REG_BCR: gpio_odr <= gpio_odr & ~s_apb_pwdata;
            GPIO_REG_OER: gpio_oer <= s_apb_pwdata;
            GPIO_REG_OESR: gpio_oer <= gpio_oer | s_apb_pwdata;
            GPIO_REG_OECR: gpio_oer <= gpio_oer & ~s_apb_pwdata;
            default: ; // nothing
            endcase
        end

        if (rst) begin
            gpio_oer <= 32'd0;
            gpio_odr <= 32'd0;
        end
    end

    assign s_apb_pready = 1'b1;
    reg [31:0] reg_rd;
    always @(*) begin
        case ({s_apb_paddr[15:2], 2'b00})
        GPIO_REG_ODR: reg_rd = gpio_odr;
        GPIO_REG_IDR: reg_rd = gpio_idr;
        GPIO_REG_OER: reg_rd = gpio_oer;
        default: reg_rd = 32'hdeadbeef;
        endcase
    end
    assign s_apb_prdata = reg_rd;

    // Input synchronizer
    reg [31:0] gpio_insync;
    always @(posedge clk) begin
        gpio_insync <= gpio_in;
        gpio_idr <= gpio_insync;
    end

    // Output
    assign gpio_out = gpio_odr;
    assign gpio_oe = gpio_oer;

endmodule

`default_nettype wire
