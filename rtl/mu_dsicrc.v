`timescale 1ns / 1ps
/* 
 * This code is based on the DSI Core
 * Copyright (C) 2013-2014 twl <twlostow@printf.cc>
 * Copyright (C) 2024 Wenting <zephray@outlook.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
 */
module mu_dsicrc #(
    parameter BYTES = 2,
    parameter COMBOUT = 1 // Combinational output, data ready the same cycle
) (
    input               clk,
    input               rst,
    input [BYTES*8-1:0] data,
    input               data_valid,
    output [15:0]       crc
);

    reg [15:0]  crc_cur;

    wire [15:0] stages_in [0:BYTES-1];
    wire [15:0] stages_out [0:BYTES-1];

    genvar i;
    generate for (i=0;i<BYTES;i=i+1) begin
        assign stages_in[i] = (i != (BYTES-1)) ? stages_out[i+1] : crc_cur;
        mu_dsicrccomb stageX(stages_in[i], data[8*i+7:8*i], stages_out[i]);
    end endgenerate

    always @(posedge clk)
        if(rst)
            crc_cur <= 16'hffff;
        else if(data_valid)
            crc_cur <= stages_out[0];

    wire [15:0] crc_out = COMBOUT ? stages_out[0] : crc_cur;

    assign crc = {crc_out[0], crc_out[1],crc_out[2], crc_out[3],
                crc_out[4], crc_out[5],crc_out[6], crc_out[7],
                crc_out[8], crc_out[9],crc_out[10], crc_out[11],
                crc_out[12], crc_out[13],crc_out[14], crc_out[15]};

endmodule
