`timescale 1ns / 1ps
`default_nettype none
`include "mu_defines.vh"
//
// mu_axiwrdma.v: Reusable AXI write-only DMA module
//
// Copyright 2024 Wenting Zhang <zephray@outlook.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
module mu_axiwrdma #(
    parameter AXI_AW = 32,
    parameter AXI_DW = 64   // This module only works with 64-bit wide DW
) (
    input  wire                 clk,    // Clock
    input  wire                 rst,    // Synchronous reset active high
    // AXI master port
    /* verilator lint_off UNUSEDSIGNAL */
    `AXI_MASTER_IF(AXI_AW, AXI_DW, 1),  // Doesn't really need ID
    /* verilator lint_on UNUSEDSIGNAL */
    // Data input
    input  wire [AXI_DW-1:0]    wr_din,
    input  wire                 wr_valid,
    output wire                 wr_ready,
    // Control
    input  wire [AXI_AW-1:0]    ctl_startaddr,
    input  wire [AXI_AW-1:0]    ctl_endaddr,
    input  wire [7:0]           ctl_burstlen,
    input  wire [7:0]           ctl_maxinflight,
    input  wire                 ctl_trigger,
    output reg                  ctl_busy,
    output reg                  ctl_error
);

    localparam ARSIZE = $clog2(AXI_DW / 8); // Pick data bus width

    // Tieoff AXI AR/R channel, not used
    assign m_axi_awid = 'd0;
    assign m_axi_araddr = 'd0;
    assign m_axi_arid = 'd0;
    assign m_axi_arlen = 'd0;
    assign m_axi_arsize = 'd0;
    assign m_axi_arburst = 'd0;
    assign m_axi_arvalid = 'd0;
    assign m_axi_rready = 'd0;

    reg [AXI_AW-1:0] awaddr;
    reg awvalid;

    assign m_axi_awid = 'd0; // This module doesn't use ID
    assign m_axi_awaddr = awaddr;
    assign m_axi_awlen = ctl_burstlen - 'd1;
    assign m_axi_awsize = ARSIZE[2:0];
    assign m_axi_awburst = `AXI_BURST_INCR;
    assign m_axi_awvalid = awvalid;

    reg wrallowed;

    assign m_axi_wlast = (burstcntr == ctl_burstlen - 'd1);
    assign m_axi_wdata = wr_din;
    assign m_axi_wvalid = wr_valid && wrallowed;
    assign wr_ready = wrallowed;
    assign m_axi_wstrb = {(AXI_DW / 8){1'b1}};

    reg [7:0] inflight;
    reg [7:0] burstcntr;

    wire awhandshake = m_axi_awvalid && m_axi_awready;
    wire whandshake = m_axi_wvalid && m_axi_wready;
    wire wburstfinish = whandshake && m_axi_wlast;
    wire bhandshake = m_axi_bready && m_axi_bvalid;
    /* verilator lint_off WIDTHEXPAND */
    wire [AXI_AW-1:0] awaddr_next = awaddr + {ctl_burstlen, {ARSIZE{1'b0}}};
    /* verilator lint_on WIDTHEXPAND */
    wire next_reached = awaddr_next == ctl_endaddr;
    reg reachend_end;
    
    always @(posedge clk) begin
        if (!ctl_busy) begin
            // Not currently active
            if (ctl_trigger) begin
               inflight <= 'd0;
               awaddr <= ctl_startaddr;
               awvalid <= 1'b1;
               wrallowed <= 1'b0;
               burstcntr <= 'd0;
               ctl_busy <= 1'b1;
               ctl_error <= 1'b0; 
               reachend_end <= 1'b0;
            end
        end
        else begin
            // Currently active

            // Finish handshake
            if (awhandshake) begin
                awvalid <= 1'b0;
                // Allow dataflow
                wrallowed <= 1'b1;
            end

            // Generate new request
            if (!awvalid && !reachend_end && (inflight < ctl_maxinflight)) begin
                awaddr <= awaddr_next;
                if (next_reached)
                    reachend_end <= 1'b1;
                else
                    awvalid <= 1'b1;
            end

            // Update inflight counter
            if (awhandshake && !wburstfinish) begin
                inflight <= inflight + 'd1;
            end
            else if (!awhandshake && wburstfinish) begin
                inflight <= inflight - 'd1;
                // Disallow dataflow if no active write is in flight
                if (inflight == 'd1)
                    wrallowed <= 1'b0;
            end

            // Update burst counter
            if (whandshake) begin
                if (m_axi_wlast)
                    burstcntr <= 'd0;
                else
                    burstcntr <= burstcntr + 'd1;
            end

            // Check write response
            if (bhandshake) begin
                if (m_axi_bresp != `AXI_RESP_OKAY) begin
                    ctl_error <= 1'b1;
                end
            end

            // Check end condition
            if (reachend_end && (inflight == 'd0)) begin
                ctl_busy <= 1'b0;
            end
        end

        if (rst) begin
            awaddr <= 'd0;
            awvalid <= 1'b0;
            wrallowed <= 1'b0;
            burstcntr <= 'd0;
            ctl_busy <= 1'b0;
            ctl_error <= 1'b0;
        end
    end

    // Doesn't currently care the b-channel
    assign m_axi_bready = 1'b1;

endmodule

`default_nettype wire
