`timescale 1ns / 1ps
`default_nettype none
`include "mu_defines.vh"
//
// mu_epdif.v: Electrophoretic Display Interface
//
// Copyright 2024 Wenting Zhang <zephray@outlook.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
module mu_epdif #(
    parameter AXI_AW = 32,
    parameter AXI_DW = 64,  // This module only works with 64-bit wide AXI
    parameter WVFM_MAX_FRAMES = 64,
    parameter MV_EN = 1, // Enable multi-voltage panel support
    parameter BW = MV_EN ? 24 : 16 // EPD data bus width
) (
    input  wire         clk,        // Clock
    input  wire         clk_pix,    // Pixel clock
    input  wire         rst,
    input  wire         rst_pix,
    // APB device port for register access
    `APB_SLAVE_IF,
    // AXI host port for direct memory access
    `AXI_MASTER_IF(AXI_AW, AXI_DW, 2), // Need 1 bit ID for 2 DMAs + 1bit waste
    // Display interface
    output reg          epd_gdoe,
    output reg          epd_gdclk,
    output reg          epd_gdsp,
    output wire         epd_sdclk,
    output reg          epd_sdle,
    output reg          epd_sdoe,
    output reg [BW-1:0] epd_sd,
    output reg          epd_sdce0
);

    localparam WVFM_FBITS = $clog2(WVFM_MAX_FRAMES);
    localparam WVFM_ABITS = WVFM_FBITS + 8;

    localparam EPDIF_REG_DCCTL          = 16'h0;
    localparam EPDIF_REG_DMACTL         = 16'h4;
    localparam EPDIF_REG_EPDCTL         = 16'h8;
    localparam EPDIF_REG_DEBUG          = 16'hC;
    localparam EPDIF_REG_VPORCH         = 16'h10;
    localparam EPDIF_REG_VACT           = 16'h14;
    localparam EPDIF_REG_HPORCH         = 16'h18;
    localparam EPDIF_REG_HACT           = 16'h1C;
    localparam EPDIF_REG_STARTADDR0_L   = 16'h20;
    localparam EPDIF_REG_STARTADDR0_H   = 16'h24;
    localparam EPDIF_REG_ENDADDR0_L     = 16'h28;
    localparam EPDIF_REG_ENDADDR0_H     = 16'h2C;
    localparam EPDIF_REG_STARTADDR1_L   = 16'h30;
    localparam EPDIF_REG_STARTADDR1_H   = 16'h34;
    localparam EPDIF_REG_ENDADDR1_L     = 16'h38;
    localparam EPDIF_REG_ENDADDR1_H     = 16'h3C;
    localparam EPDIF_REG_LUTADDRCLR     = 16'h40;
    localparam EPDIF_REG_LUTWR          = 16'h44;

    localparam FBMODE_DUAL              = 2'd0;
    localparam FBMODE_ONLY_FB0          = 2'd1;
    localparam FBMODE_ONLY_FB1          = 2'd2;
    localparam FBMODE_NONE              = 2'd3;

    /* verilator lint_off UNUSEDSIGNAL */
    reg [31:0] epdif_dcctl;
    reg [15:0] epdif_dmactl;
    reg [7:0] epdif_epdctl;
    reg [BW-1:0] epdif_debug;
    reg [23:0] epdif_vporch;
    reg [11:0] epdif_vact;
    reg [23:0] epdif_hporch;
    reg [11:0] epdif_hact;
    reg [31:0] epdif_startaddr0_l;
    reg [31:0] epdif_startaddr0_h;
    reg [31:0] epdif_endaddr0_l;
    reg [31:0] epdif_endaddr0_h;
    reg [31:0] epdif_startaddr1_l;
    reg [31:0] epdif_startaddr1_h;
    reg [31:0] epdif_endaddr1_l;
    reg [31:0] epdif_endaddr1_h;
    /* verilator lint_on UNUSEDSIGNAL */


    reg apb_ready;

    reg [31:0] lutwrfifo_data;
    reg lutwrfifo_valid;
    wire lutwrfifo_ready;

    // Handshaking to reset LUT WR address
    reg lutwr_reset_req;
    wire lutwr_reset_ack;

    // APB Access
    always @(posedge clk) begin
        // Unless blocked by LUTWR FIFO, mark APB as ready
        if (!lutwrfifo_valid || lutwrfifo_ready)
            apb_ready <= 1'b1;

        if (lutwrfifo_valid && lutwrfifo_ready)
            lutwrfifo_valid <= 1'b0;

        if (s_apb_penable && s_apb_pwrite && s_apb_psel) begin
            case (s_apb_paddr[15:0])
            EPDIF_REG_DCCTL: epdif_dcctl <= s_apb_pwdata;
            EPDIF_REG_DMACTL: epdif_dmactl <= s_apb_pwdata[15:0];
            EPDIF_REG_EPDCTL: epdif_epdctl <= s_apb_pwdata[7:0];
            EPDIF_REG_DEBUG: epdif_debug <= s_apb_pwdata[BW-1:0];
            EPDIF_REG_VPORCH: epdif_vporch <= s_apb_pwdata[23:0];
            EPDIF_REG_VACT: epdif_vact <= s_apb_pwdata[11:0];
            EPDIF_REG_HPORCH: epdif_hporch <= s_apb_pwdata[23:0];
            EPDIF_REG_HACT: epdif_hact <= s_apb_pwdata[11:0];
            EPDIF_REG_STARTADDR0_L: epdif_startaddr0_l <= s_apb_pwdata;
            EPDIF_REG_STARTADDR0_H: epdif_startaddr0_h <= s_apb_pwdata;
            EPDIF_REG_ENDADDR0_L: epdif_endaddr0_l <= s_apb_pwdata;
            EPDIF_REG_ENDADDR0_H: epdif_endaddr0_h <= s_apb_pwdata;
            EPDIF_REG_STARTADDR1_L: epdif_startaddr1_l <= s_apb_pwdata;
            EPDIF_REG_STARTADDR1_H: epdif_startaddr1_h <= s_apb_pwdata;
            EPDIF_REG_ENDADDR1_L: epdif_endaddr1_l <= s_apb_pwdata;
            EPDIF_REG_ENDADDR1_H: epdif_endaddr1_h <= s_apb_pwdata;
            EPDIF_REG_LUTADDRCLR: lutwr_reset_req <= s_apb_pwdata[0];
            EPDIF_REG_LUTWR: begin
                lutwrfifo_data <= s_apb_pwdata;
                lutwrfifo_valid <= 1'b1;
            end
            default: ;
            endcase
        end
        else begin
            // Register state updates
            if (epd_update_done) begin
                epdif_dcctl[0] <= 1'b0; // Clear enable signal
            end
            if (lutwr_reset_ack) begin
                lutwr_reset_req <= 1'b0;
            end
        end

        if (rst) begin
            epdif_dcctl <= 32'd0;
            epdif_dmactl <= 16'd0;
        end
    end

    assign s_apb_pready = apb_ready;
    reg [31:0] reg_rd;
    always @(*) begin
        case ({s_apb_paddr[15:2], 2'b0})
        EPDIF_REG_DCCTL: reg_rd = epdif_dcctl;
        EPDIF_REG_DMACTL: reg_rd = {16'd0, epdif_dmactl};
        EPDIF_REG_EPDCTL: reg_rd = {24'd0, epdif_epdctl};
        EPDIF_REG_VPORCH: reg_rd = {8'd0, epdif_vporch};
        EPDIF_REG_VACT: reg_rd = {20'd0, epdif_vact};
        EPDIF_REG_HPORCH: reg_rd = {8'd0, epdif_hporch};
        EPDIF_REG_HACT: reg_rd = {20'd0, epdif_hact};
        EPDIF_REG_STARTADDR0_L: reg_rd = epdif_startaddr0_l;
        EPDIF_REG_STARTADDR0_H: reg_rd = epdif_startaddr0_h;
        EPDIF_REG_ENDADDR0_L: reg_rd = epdif_endaddr0_l;
        EPDIF_REG_ENDADDR0_H: reg_rd = epdif_endaddr0_h;
        EPDIF_REG_STARTADDR1_L: reg_rd = epdif_startaddr1_l;
        EPDIF_REG_STARTADDR1_H: reg_rd = epdif_startaddr1_h;
        EPDIF_REG_ENDADDR1_L: reg_rd = epdif_endaddr1_l;
        EPDIF_REG_ENDADDR1_H: reg_rd = epdif_endaddr1_h;
        EPDIF_REG_LUTADDRCLR: reg_rd = {31'd0, lutwr_reset_req};
        default: reg_rd = 32'hdeadbeef;
        endcase
    end
    assign s_apb_prdata = reg_rd;

    // Register fields
    // Fields used in clk domain
    wire dc_8ppc = epdif_dcctl[5]; // 0 - 4pix per cycle, 1 - 8pix per cycle
    wire [1:0] fbmode = epdif_dcctl[7:6];
    wire [7:0] ctl_burstlen = epdif_dmactl[7:0];
    wire [7:0] ctl_maxinflight = epdif_dmactl[15:8];

    // Fields used in clk_pix domain
    /* verilator lint_off UNUSEDSIGNAL */
    wire [31:0] epdif_dcctl_clk_pix;
    wire [7:0] epdif_epdctl_clk_pix;
    wire [BW-1:0] epdif_debug_clk_pix;
    wire [23:0] epdif_vporch_clk_pix;
    wire [11:0] epdif_vact_clk_pix;
    wire [23:0] epdif_hporch_clk_pix;
    wire [11:0] epdif_hact_clk_pix;
    /* verilator lint_on UNUSEDSIGNAL */
    mu_dbsync #(32) dcctl_sync (clk, clk_pix, epdif_dcctl, epdif_dcctl_clk_pix);
    mu_dbsync #(8) epdctl_sync (clk, clk_pix, epdif_epdctl, epdif_epdctl_clk_pix);
    mu_dbsync #(BW) debug_sync (clk, clk_pix, epdif_debug, epdif_debug_clk_pix);
    mu_dbsync #(24) vporch_sync (clk, clk_pix, epdif_vporch, epdif_vporch_clk_pix);
    mu_dbsync #(12) vact_sync (clk, clk_pix, epdif_vact, epdif_vact_clk_pix);
    mu_dbsync #(24) hporch_sync (clk, clk_pix, epdif_hporch, epdif_hporch_clk_pix);
    mu_dbsync #(12) hact_sync (clk, clk_pix, epdif_hact, epdif_hact_clk_pix);

    wire tgen_en = epdif_dcctl_clk_pix[0];
    wire [WVFM_FBITS-1:0] wvfm_length = epdif_epdctl_clk_pix[WVFM_FBITS-1:0];
    wire out_en = epdif_dcctl_clk_pix[10];
    wire outdbg_en = epdif_dcctl_clk_pix[8];
    wire revpol = epdif_dcctl_clk_pix[9];
    wire [3:0] fb0_fixedval = epdif_dcctl_clk_pix[27:24];
    wire [3:0] fb1_fixedval = epdif_dcctl_clk_pix[31:28];

    wire [7:0] vfp = epdif_vporch_clk_pix[7:0];
    wire [7:0] vsync = epdif_vporch_clk_pix[15:8];
    wire [7:0] vbp = epdif_vporch_clk_pix[23:16];
    wire [11:0] vact = epdif_vact_clk_pix[11:0];
    wire [7:0] hfp = epdif_hporch_clk_pix[7:0];
    wire [7:0] hsync = epdif_hporch_clk_pix[15:8];
    wire [7:0] hbp = epdif_hporch_clk_pix[23:16];
    wire [11:0] hact = epdif_hact_clk_pix[11:0];

    // DMA Control
    wire [AXI_AW-1:0] ctl_startaddr0;
    wire [AXI_AW-1:0] ctl_endaddr0;
    wire [AXI_AW-1:0] ctl_startaddr1;
    wire [AXI_AW-1:0] ctl_endaddr1;
    generate
        if (AXI_AW > 32) begin: gen_addr_64
            assign ctl_startaddr0 =
                {epdif_startaddr0_h[AXI_AW-32-1:0], epdif_startaddr0_l};
            assign ctl_endaddr0 =
                {epdif_endaddr0_h[AXI_AW-32-1:0], epdif_endaddr0_l};
            assign ctl_startaddr1 =
                {epdif_startaddr1_h[AXI_AW-32-1:0], epdif_startaddr1_l};
            assign ctl_endaddr1 =
                {epdif_endaddr1_h[AXI_AW-32-1:0], epdif_endaddr1_l};
        end
        else begin: gen_addr_32
            assign ctl_startaddr0 = epdif_startaddr0_l[AXI_AW-1:0];
            assign ctl_endaddr0 = epdif_endaddr0_l[AXI_AW-1:0];
            assign ctl_startaddr1 = epdif_startaddr1_l[AXI_AW-1:0];
            assign ctl_endaddr1 = epdif_endaddr1_l[AXI_AW-1:0];
        end
    endgenerate

    // AXI crossbar for RDDMA
    `AXI_WIRES(dma0_, AXI_AW, AXI_DW, 1);
    `AXI_WIRES(dma1_, AXI_AW, AXI_DW, 1);

    /* verilator lint_off PINMISSING */
    axi_crossbar_wrap_2x1 #(
        .DATA_WIDTH(AXI_DW),
        .ADDR_WIDTH(AXI_AW),
        .S_ID_WIDTH(1),
        .M00_ADDR_WIDTH(32'd32)
    ) axi_crossbar_wrap_2x1 (
        .clk(clk),
        .rst(rst),
        `AXI_CONN(s00_, dma0_),
        `AXI_CONN(s01_, dma1_),
        `AXI_CONN(m00_, m_)
    );
    /* verilator lint_on PINMISSING */

    wire [AXI_DW-1:0] dma0rd_dout;
    wire dma0rd_valid;
    wire dma0rd_ready;
    wire [AXI_DW-1:0] dma1rd_dout;
    wire dma1rd_valid;
    wire dma1rd_ready;

    reg ctl_trigger; // internal DMA trigger

    wire ctl_trigger_apb_clk;
    mu_dsync trigger_sync (clk_pix, clk, ctl_trigger, ctl_trigger_apb_clk);
    wire ctl_trigger0 = ctl_trigger_apb_clk &&
        ((fbmode == FBMODE_DUAL) || (fbmode == FBMODE_ONLY_FB0));
    wire ctl_trigger1 = ctl_trigger_apb_clk &&
        ((fbmode == FBMODE_DUAL) || (fbmode == FBMODE_ONLY_FB1));

    mu_axirddma mu_axirddma0 (
        .clk(clk),
        .rst(rst),
        `AXI_CONN(m_, dma0_),
        // Data output
        .rd_dout(dma0rd_dout),
        .rd_valid(dma0rd_valid),
        .rd_ready(dma0rd_ready),
        // Control
        .ctl_startaddr(ctl_startaddr0),
        .ctl_endaddr(ctl_endaddr0),
        .ctl_burstlen(ctl_burstlen),
        .ctl_maxinflight(ctl_maxinflight),
        .ctl_trigger(ctl_trigger0),
        .ctl_busy(),
        .ctl_error()
    );

    mu_axirddma mu_axirddma1 (
        .clk(clk),
        .rst(rst),
        `AXI_CONN(m_, dma1_),
        // Data output
        .rd_dout(dma1rd_dout),
        .rd_valid(dma1rd_valid),
        .rd_ready(dma1rd_ready),
        // Control
        .ctl_startaddr(ctl_startaddr1),
        .ctl_endaddr(ctl_endaddr1),
        .ctl_burstlen(ctl_burstlen),
        .ctl_maxinflight(ctl_maxinflight),
        .ctl_trigger(ctl_trigger1),
        .ctl_busy(),
        .ctl_error()
    );

    // First width adapt to 32b (depending on AXI width)
    // This is non-bypassable
    wire [31:0] dma0rd32b_dout;
    wire dma0rd32b_valid;
    wire dma0rd32b_ready;
    wire [31:0] dma1rd32b_dout;
    wire dma1rd32b_valid;
    wire dma1rd32b_ready;
    mu_widthadapt_2_to_1 #(
        .IW(64),
        .OW(32)
    ) pix_width_adapt_stage1_0 (
        .clk(clk),
        .rst(rst),
        // Incoming port
        //.wr_data(dma0rd_dout),
        .wr_data({
            dma0rd_dout[7:0],
            dma0rd_dout[15:8],
            dma0rd_dout[23:16],
            dma0rd_dout[31:24],
            dma0rd_dout[39:32],
            dma0rd_dout[47:40],
            dma0rd_dout[55:48],
            dma0rd_dout[63:56]
        }),
        .wr_valid(dma0rd_valid),
        .wr_ready(dma0rd_ready),
        // Outgoing port
        .rd_data(dma0rd32b_dout),
        .rd_valid(dma0rd32b_valid),
        .rd_ready(dma0rd32b_ready)
    );

    mu_widthadapt_2_to_1 #(
        .IW(64),
        .OW(32)
    ) pix_width_adapt_stage1_1 (
        .clk(clk),
        .rst(rst),
        // Incoming port
        //.wr_data(dma1rd_dout),
        .wr_data({
            dma1rd_dout[7:0],
            dma1rd_dout[15:8],
            dma1rd_dout[23:16],
            dma1rd_dout[31:24],
            dma1rd_dout[39:32],
            dma1rd_dout[47:40],
            dma1rd_dout[55:48],
            dma1rd_dout[63:56]
        }),
        .wr_valid(dma1rd_valid),
        .wr_ready(dma1rd_ready),
        // Outgoing port
        .rd_data(dma1rd32b_dout),
        .rd_valid(dma1rd32b_valid),
        .rd_ready(dma1rd32b_ready)
    );

    // The widthadapt can be optionally bypassed
    wire dma0rd16b_valid = dc_8ppc ? 1'b0 : dma0rd32b_valid;
    wire dma0rd16b_ready;
    assign dma0rd32b_ready = dc_8ppc ? dma0fifo_ready : dma0rd16b_ready;

    wire dma1rd16b_valid = dc_8ppc ? 1'b0 : dma1rd32b_valid;
    wire dma1rd16b_ready;
    assign dma1rd32b_ready = dc_8ppc ? dma1fifo_ready : dma1rd16b_ready;

    wire dma0pix16b_valid;
    wire dma0pix16b_ready = dc_8ppc ? 1'b0 : dma0fifo_ready;
    wire [15:0] dma0pix16b_data;
    
    wire dma1pix16b_valid;
    wire dma1pix16b_ready = dc_8ppc ? 1'b0 : dma1fifo_ready;
    wire [15:0] dma1pix16b_data;

    mu_widthadapt_2_to_1 #(
        .IW(32),
        .OW(16)
    ) pix_width_adapt_stage2_0 (
        .clk(clk),
        .rst(rst),
        // Incoming port
        .wr_data(dma0rd32b_dout),
        .wr_valid(dma0rd16b_valid),
        .wr_ready(dma0rd16b_ready),
        // Outgoing port
        .rd_data(dma0pix16b_data),
        .rd_valid(dma0pix16b_valid),
        .rd_ready(dma0pix16b_ready)
    );

    mu_widthadapt_2_to_1 #(
        .IW(32),
        .OW(16)
    ) pix_width_adapt_stage2_1 (
        .clk(clk),
        .rst(rst),
        // Incoming port
        .wr_data(dma1rd32b_dout),
        .wr_valid(dma1rd16b_valid),
        .wr_ready(dma1rd16b_ready),
        // Outgoing port
        .rd_data(dma1pix16b_data),
        .rd_valid(dma1pix16b_valid),
        .rd_ready(dma1pix16b_ready)
    );

    wire [31:0] dma0fifo_data = dc_8ppc ? dma0rd32b_dout : {16'b0, dma0pix16b_data};
    wire dma0fifo_valid = dc_8ppc ? dma0rd32b_valid : dma0pix16b_valid;
    wire dma0fifo_ready;

    wire [31:0] dma1fifo_data = dc_8ppc ? dma1rd32b_dout : {16'b0, dma1pix16b_data};
    wire dma1fifo_valid = dc_8ppc ? dma1rd32b_valid : dma1pix16b_valid;
    wire dma1fifo_ready;

    wire [31:0] fb0_pixel;
    wire [31:0] fb1_pixel;

    // TODO: FIFO underrun detect
    wire fb_rden;
    wire fb0_valid;
    wire fb1_valid;

    mu_fifo_async #(.DW(32), .DEPTH(512)) fb0_fifo (
        .wr_clk(clk),
        .wr_nreset(!rst),
        .wr_din(dma0fifo_data),
        .wr_valid(dma0fifo_valid),
        .wr_ready(dma0fifo_ready),
        .rd_clk(clk_pix),
        .rd_nreset(!rst_pix),
        .rd_dout(fb0_pixel),
        .rd_ready(fb_rden),
        .rd_valid(fb0_valid)
    );

    mu_fifo_async #(.DW(32), .DEPTH(512)) fb1_fifo (
        .wr_clk(clk),
        .wr_nreset(!rst),
        .wr_din(dma1fifo_data),
        .wr_valid(dma1fifo_valid),
        .wr_ready(dma1fifo_ready),
        .rd_clk(clk_pix),
        .rd_nreset(!rst_pix),
        .rd_dout(fb1_pixel),
        .rd_ready(fb_rden),
        .rd_valid(fb1_valid)
    );

    // Timing Gen and Processing
    /* verilator lint_off WIDTHEXPAND */
    wire [12:0] vtotal = vfp + vsync + vbp + vact;
    wire [12:0] htotal = hfp + hsync + hbp + hact;
    /* verilator lint_on WIDTHEXPAND */

    reg [12:0] scan_v_cnt;
    reg [12:0] scan_h_cnt;

    reg [1:0] scan_state;
    localparam SCAN_IDLE = 2'd0;
    localparam SCAN_RUNNING = 2'd1;
    localparam SCAN_DONE = 2'd2;

    reg [WVFM_FBITS-1:0] wvfm_frame_cntr;

    always @(posedge clk_pix) begin
        case (scan_state)
        SCAN_IDLE: begin
            if (tgen_en) begin
                scan_state <= SCAN_RUNNING;
                ctl_trigger <= 1'b1;
                wvfm_frame_cntr <= 0;
            end
            scan_h_cnt <= 0;
            scan_v_cnt <= 0;
        end
        SCAN_RUNNING: begin
            ctl_trigger <= 1'b0;
            if (scan_h_cnt == htotal - 1) begin
                if (scan_v_cnt == vtotal - 1) begin
                    scan_h_cnt <= 'd0;
                    scan_v_cnt <= 'd0;
                    if (wvfm_frame_cntr < wvfm_length) begin
                        // Update not done yet, continue
                        ctl_trigger <= 1'b1;
                        wvfm_frame_cntr <= wvfm_frame_cntr + 'd1;
                    end
                    else begin
                        // Update done
                        scan_state <= SCAN_DONE;
                    end
                end
                else begin
                    scan_h_cnt <= 0;
                    scan_v_cnt <= scan_v_cnt + 1;
                end
            end
            else begin
                scan_h_cnt <= scan_h_cnt + 1;
            end
        end
        SCAN_DONE: begin
            if (!tgen_en)
                scan_state <= SCAN_IDLE;
        end
        default: begin
            // Invalid state
            $display("Scan FSM in invalid state");
            scan_state <= SCAN_IDLE;
        end
        endcase

        if (rst) begin
            scan_state <= SCAN_IDLE;
            scan_h_cnt <= 0;
            scan_v_cnt <= 0;
        end
    end

    wire epd_update_done_clk_pix = scan_state == SCAN_DONE;
    wire epd_update_done;
    mu_dsync scan_done_sync (clk_pix, clk, epd_update_done_clk_pix, epd_update_done);

    /* verilator lint_off width */
    wire scan_in_vsync = (scan_state != SCAN_IDLE) ? (
        (scan_v_cnt >= vfp) &&
        (scan_v_cnt < (vfp + vsync))) : 1'b0;
    wire scan_in_vbp = (scan_state != SCAN_IDLE) ? (
        (scan_v_cnt >= (vfp + vsync)) &&
        (scan_v_cnt < (vfp + vsync + vbp))) : 1'b0;
    wire scan_in_vact = (scan_state != SCAN_IDLE) ? (
        (scan_v_cnt >= (vfp + vsync + vbp))) : 1'b0;

    wire scan_in_hfp = (scan_state != SCAN_IDLE) ? (
        (scan_h_cnt < hfp)) : 1'b0;
    wire scan_in_hsync = (scan_state != SCAN_IDLE) ? (
        (scan_h_cnt >= hfp) &&
        (scan_h_cnt < (hfp + hsync))) : 1'b0;
    wire scan_in_drst = (scan_state != SCAN_IDLE) ? (
        (scan_h_cnt >= (hfp + 10)) &&
        (scan_h_cnt < (hfp + hsync + hbp - 2))) : 1'b0;
    wire scan_in_hbp = (scan_state != SCAN_IDLE) ? (
        (scan_h_cnt >= (hfp + hsync)) &&
        (scan_h_cnt < (hfp + hsync + hbp))) : 1'b0;
    wire scan_in_hact = (scan_state != SCAN_IDLE) ? (
        (scan_h_cnt >= (hfp + hsync + hbp))) : 1'b0;
    /* verilator lint_on width */

    wire scan_in_act = scan_in_vact && scan_in_hact;

    // Processing pipeline: 1 stage
    // Stage 1: FIFO readout -> Waveform lookup
    localparam PIPELINE_DELAY = 1;
    /* verilator lint_off width */
    wire s1_hactive = (scan_state != SCAN_IDLE) ? (
        (scan_h_cnt >= (hfp + hsync + hbp - PIPELINE_DELAY)) &&
        (scan_h_cnt < (htotal - PIPELINE_DELAY))) : 1'b0;
    /* verilator lint_on width */
    wire s1_active = scan_in_vact && s1_hactive;

    assign fb_rden = s1_active;

    reg [31:0] fb0_val;
    reg [31:0] fb1_val;
    always @(*) begin
        case (fbmode)
        FBMODE_DUAL: begin
            fb0_val = fb0_pixel;
            fb1_val = fb1_pixel;
        end
        FBMODE_ONLY_FB0: begin
            fb0_val = fb0_pixel;
            fb1_val = {8{fb1_fixedval}};
        end
        FBMODE_ONLY_FB1: begin
            fb0_val = {8{fb0_fixedval}};
            fb1_val = fb1_pixel;
        end
        FBMODE_NONE: begin
            fb0_val = {8{fb0_fixedval}};
            fb1_val = {8{fb1_fixedval}};
        end
        endcase
    end

    localparam PDW = MV_EN ? 3 : 2; // pixel width
    localparam LDW = MV_EN ? 4 : 2; // LUT width
    localparam LWW = LDW * 4; // LUT write width

    wire [31:0] lutwrfifo_rd_data;
    wire lutwrfifo_rd_valid;
    wire lutwrfifo_rd_ready;
    mu_fifo_async #(.DW(32), .DEPTH(16)) lutwr_fifo (
        .wr_clk(clk),
        .wr_nreset(!rst),
        .wr_din(lutwrfifo_data),
        .wr_valid(lutwrfifo_valid),
        .wr_ready(lutwrfifo_ready),
        .rd_clk(clk_pix),
        .rd_nreset(!rst_pix),
        .rd_dout(lutwrfifo_rd_data),
        .rd_ready(lutwrfifo_rd_ready),
        .rd_valid(lutwrfifo_rd_valid)
    );

    wire wvfmlut_we = lutwrfifo_rd_valid;
    wire [LWW-1:0] wvfmlut_wr = lutwrfifo_rd_data[LWW-1:0];
    reg [WVFM_ABITS-3:0] wvfmlut_waddr;
    assign lutwrfifo_rd_ready = 1'b1;

    wire lutwr_reset_req_clk_pix;
    reg lutwr_reset_ack_clk_pix;
    mu_dsync rst_req_sync (clk, clk_pix, lutwr_reset_req, lutwr_reset_req_clk_pix);
    mu_dsync rst_ack_sync (clk_pix, clk, lutwr_reset_ack_clk_pix, lutwr_reset_ack);

    always @(posedge clk) begin
        if (lutwr_reset_req_clk_pix) begin
            lutwr_reset_ack_clk_pix <= 1'b1;
            wvfmlut_waddr <= 'd0;
        end
        else begin
            lutwr_reset_ack_clk_pix <= 1'b0;
            if (wvfmlut_we)
                wvfmlut_waddr <= wvfmlut_waddr + 'd1;
        end

        if (rst) begin
            wvfmlut_waddr <= 'd0;
            lutwr_reset_ack_clk_pix <= 1'b0;
        end
    end

    wire [BW-1:0] s1_dout;

    genvar i;
    generate for (i = 0; i < 4; i = i + 1) begin
        wire [LDW-1:0] douta;
        wire [LDW-1:0] doutb;
        mu_epdif_wvfmlut #(
            .AW(WVFM_ABITS),
            .DW(LDW)
        ) lut (
            .clk(clk_pix),
            .we(wvfmlut_we),
            .addr(wvfmlut_waddr),
            .din(wvfmlut_wr),
            .addra({wvfm_frame_cntr, fb1_val[(i*8)+:4], fb0_val[(i*8)+:4]}),
            .douta(douta),
            .addrb({wvfm_frame_cntr, fb1_val[(i*8+4)+:4], fb0_val[(i*8+4)+:4]}),
            .doutb(doutb)
        );
        assign s1_dout[(i*2*PDW)+:PDW] = douta[PDW-1:0];
        assign s1_dout[((i*2+1)*PDW)+:PDW] = doutb[PDW-1:0];
    end endgenerate

    // Signal output
    reg epd_gdclk_pre;
    always @(posedge clk_pix) begin
        //epd_gdoe <= out_en && (scan_in_vsync || scan_in_vbp || scan_in_vact);
        epd_gdoe <= out_en;
        epd_gdclk_pre <= out_en && (scan_in_hsync || scan_in_hbp || scan_in_hact);
        epd_gdclk <= out_en && epd_gdclk_pre;
        epd_gdsp <= out_en && !scan_in_vsync;
        epd_sdle <= out_en && scan_in_hsync;
        //epd_sdle <= 1'b0;
        //epd_sdoe <= out_en && (scan_in_vsync || scan_in_vbp || scan_in_vact);
        epd_sdoe <= out_en;
        //epd_sdce0 <= out_en && !scan_in_act;
        epd_sdce0 <= 1'b0;
        epd_sd <= {BW{revpol}} ^
            (scan_in_drst ? {BW{1'b1}} :
            scan_in_act ? (outdbg_en ? epdif_debug_clk_pix : s1_dout) :
            'd0);
    end
    assign epd_sdclk = out_en && clk_pix;

endmodule

`default_nettype wire
